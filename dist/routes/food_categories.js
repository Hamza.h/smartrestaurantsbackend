"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const food_categories_controller_1 = require("../controllers/food_categories_controller");
const router = express_1.Router();
router.get("/", food_categories_controller_1.getFoodCategories);
exports.default = router;
