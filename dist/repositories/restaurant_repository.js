"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantRepository = void 0;
const typeorm_1 = require("typeorm");
const restaurant_1 = __importDefault(require("../models/restaurant"));
const food_category_1 = __importDefault(require("../models/food_category"));
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
let RestaurantRepository = class RestaurantRepository extends typeorm_1.Repository {
    addFoodCategories(ids, restaurant) {
        const foodCategoryRepository = typeorm_1.getRepository(food_category_1.default);
        const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
        return foodCategoryRepository.findByIds(ids).then((foodCategories) => {
            foodCategories.forEach((foodCategory) => __awaiter(this, void 0, void 0, function* () {
                const restaurantFoodCategory = new restaurant_food_category_1.default();
                restaurantFoodCategory.foodCategory = foodCategory;
                restaurantFoodCategory.restaurant = restaurant;
                yield restaurantFoodCategoryRepository.save(restaurantFoodCategory);
            }));
        });
    }
    getAllFoodCategories(restaurant) {
        return __awaiter(this, void 0, void 0, function* () {
            const foodCategoryRepository = typeorm_1.getRepository(food_category_1.default);
            const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
            return yield restaurantFoodCategoryRepository
                .find({
                where: {
                    restaurant: restaurant,
                },
                relations: ["foodCategory"],
            })
                .then((restaurantFoodCategories) => __awaiter(this, void 0, void 0, function* () {
                return yield foodCategoryRepository.findByIds(restaurantFoodCategories.map((restaurantFoodCategory) => restaurantFoodCategory.foodCategory));
            }));
        });
    }
};
RestaurantRepository = __decorate([
    typeorm_1.EntityRepository(restaurant_1.default)
], RestaurantRepository);
exports.RestaurantRepository = RestaurantRepository;
