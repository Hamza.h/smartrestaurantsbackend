"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app_router_1 = __importDefault(require("./routes/app_router"));
const database_1 = __importDefault(require("./utils/database"));
const basics_1 = require("./middlewares/basics");
const app = express_1.default();
app.use(basics_1.CROS_MIDDLEWARE);
app.get("/favicon.ico", (req, res) => res.status(204));
app.use(app_router_1.default);
app.use("/", (req, res, next) => {
    res.status(404).json({ message: "no such route" });
});
database_1.default.then((res) => app.listen(process.env.PORT || 3000));
