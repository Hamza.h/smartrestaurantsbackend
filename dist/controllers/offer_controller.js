"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMostOrderedOffers = exports.getAllOffers = exports.searchOffer = exports.getOfferByID = exports.deleteOffer = exports.editOffer = exports.createOffer = void 0;
const typeorm_1 = require("typeorm");
const offer_1 = __importDefault(require("../models/offer"));
const meal_1 = __importDefault(require("../models/meal"));
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
const order_meal_1 = __importDefault(require("../models/order_meal"));
const constants_1 = require("../utils/constants");
const createOffer = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const offerName = req.body.offerName;
        const price = req.body.price;
        let imageUrl = req.body.imageUrl;
        const startDate = req.body.startDate;
        const endDate = req.body.endDate;
        const description = req.body.description;
        const mealsIDs = req.body.meals;
        if (!offerName)
            return res.status(400).json({ message: "Missing offerName" });
        if (!price)
            return res.status(400).json({ message: "Missing price" });
        if (!startDate)
            return res.status(400).json({ message: "Missing startDate" });
        if (!endDate)
            return res.status(400).json({ message: "Missing endDate" });
        if (!description)
            return res.status(400).json({ message: "Missing description" });
        if (!mealsIDs)
            return res.status(400).json({ message: "Missing meals" });
        const meals = mealsIDs.map((id) => {
            return { id: id };
        });
        if (!imageUrl)
            imageUrl = constants_1.defaultMealOfferImageUrl;
        typeorm_1.getRepository(offer_1.default)
            .save({
            offerName: offerName,
            price: price,
            imageUrl: imageUrl,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            description: description,
            restaurant: { id: req.body.restaurant.id },
            meals: meals,
        })
            .then((result) => {
            return res.status(200).json({ message: "Success" });
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        res.status(500).json({ message: error.message });
    }
});
exports.createOffer = createOffer;
const editOffer = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const offerId = req.body.offerId;
        const offerName = req.body.offerName;
        const price = req.body.price;
        const imageUrl = req.body.imageUrl;
        const startDate = req.body.startDate;
        const endDate = req.body.endDate;
        const description = req.body.description;
        const mealsIDs = req.body.meals;
        if (!offerId)
            return res.status(400).json({ message: "Missing offerId" });
        if (!offerName)
            return res.status(400).json({ message: "Missing offerName" });
        if (!price)
            return res.status(400).json({ message: "Missing price" });
        if (!startDate)
            return res.status(400).json({ message: "Missing startDate" });
        if (!endDate)
            return res.status(400).json({ message: "Missing endDate" });
        if (!description)
            return res.status(400).json({ message: "Missing description" });
        if (!mealsIDs)
            return res.status(400).json({ message: "Missing meals" });
        const offerRepository = typeorm_1.getRepository(offer_1.default);
        offerRepository
            .findOne(offerId, {
            relations: ["meals"],
        })
            .then((offer) => __awaiter(void 0, void 0, void 0, function* () {
            if (offer) {
                offer.meals = yield typeorm_1.getRepository(meal_1.default).findByIds(mealsIDs);
                offer.offerName = offerName;
                offer.price = price;
                offer.description = description;
                offer.startDate = new Date(startDate);
                offer.endDate = new Date(endDate);
                if (imageUrl)
                    offer.imageUrl = imageUrl;
                offerRepository
                    .save(offer)
                    .then((result) => {
                    return res.status(200).json({ message: "Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "Offer Not Found" });
        }))
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        res.status(500).json({ message: error.message });
    }
});
exports.editOffer = editOffer;
const deleteOffer = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const offerId = req.query.offerId;
        if (!offerId)
            return res.status(400).json({ message: "Missing offerId" });
        const offerRepository = typeorm_1.getRepository(offer_1.default);
        offerRepository
            .findOne(offerId, {
            relations: ["restaurant"],
        })
            .then((offer) => {
            if (offer) {
                if (offer.restaurant.id == req.body.restaurant.id)
                    offerRepository
                        .softDelete(offer)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        return res.status(500).json({ message: error.message });
                    });
            }
            else
                return res.status(404).json({ message: "Offer Not Found" });
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.deleteOffer = deleteOffer;
const getOfferByID = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const offerId = req.query.offerId;
        if (!offerId)
            return res.status(400).json({ message: "Missing offerId" });
        const offerRepository = typeorm_1.getRepository(offer_1.default);
        offerRepository
            .findOne(offerId, {
            relations: ["meals", "restaurant"],
        })
            .then((offer) => {
            if (offer)
                return res.status(200).json(offer);
            else
                return res.status(404).json({ message: "Offer Not Found" });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getOfferByID = getOfferByID;
const searchOffer = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const offerName = req.query.offerName;
        if (!offerName)
            return res.status(400).json({ message: "Missing offerName" });
        typeorm_1.getRepository(offer_1.default)
            .createQueryBuilder()
            .select()
            .where("offer_name like :label", { label: `%${offerName}%` })
            .getMany()
            .then((offers) => {
            return res.status(200).json(offers);
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.searchOffer = searchOffer;
const getAllOffers = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const page = (_a = req.query.page) !== null && _a !== void 0 ? _a : 1;
        const pageCount = 10;
        let restaurantsIDs = [];
        if (req.body.restaurant) {
            restaurantsIDs.push(req.body.restaurant.id);
        }
        else {
            const customerFoodCategories = req.body.customer.foodCategories.map((e) => e.id);
            restaurantsIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default)
                .createQueryBuilder()
                .select("restaurant_id")
                .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
                .getRawMany()).map((e) => e.restaurant_id);
        }
        typeorm_1.getRepository(offer_1.default)
            .createQueryBuilder("offer")
            .leftJoinAndSelect("offer.restaurant", "restaurant")
            .select()
            .where("restaurant_id IN (:ids)", {
            ids: restaurantsIDs,
        })
            .skip((page - 1) * pageCount)
            .take(pageCount)
            .getMany()
            .then((meals) => {
            return res.status(200).json(meals);
        })
            .catch((err) => res.status(500).json({ message: err.message }));
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getAllOffers = getAllOffers;
const getMostOrderedOffers = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let restaurantsIDs = [];
        if (req.body.restaurant) {
            restaurantsIDs.push(req.body.restaurant.id);
        }
        else {
            const customerFoodCategories = req.body.customer.foodCategories.map((e) => e.id);
            restaurantsIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default)
                .createQueryBuilder()
                .select("restaurant_id")
                .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
                .getRawMany()).map((e) => e.restaurant_id);
        }
        const offerRepository = typeorm_1.getRepository(offer_1.default);
        const orderMealRepository = typeorm_1.getRepository(order_meal_1.default);
        const whereCond = "restaurant_id IN (:ids)";
        let whereValue = { ids: restaurantsIDs };
        offerRepository
            .createQueryBuilder()
            .where(whereCond, whereValue)
            .getMany()
            .then((allOffers) => {
            if (allOffers.length > 0) {
                orderMealRepository
                    .createQueryBuilder()
                    .select("offer_id as offerId")
                    .addSelect("SUM(item_quantity) AS itemQuantity")
                    .where("offer_id IN (:offers)", {
                    offers: allOffers.map((e) => e.id),
                })
                    .orderBy("itemQuantity", "DESC")
                    .groupBy("offer_id")
                    .getRawMany()
                    .then((items) => {
                    offerRepository
                        .findByIds(items.map((e) => e.offerId), {
                        take: 3,
                    })
                        .then((offers) => {
                        return res.status(200).json(offers);
                    })
                        .catch((err) => {
                        return res.status(500).json({ message: err.message });
                    });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "No Offers Found" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getMostOrderedOffers = getMostOrderedOffers;
