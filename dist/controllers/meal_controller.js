"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMostOrderedMeals = exports.getAllReviews = exports.getAllMeals = exports.searchMeal = exports.getMealByID = exports.reviewMeal = exports.favouriteMeal = exports.deleteMeal = exports.editMeal = exports.addMeal = void 0;
const typeorm_1 = require("typeorm");
const meal_1 = __importDefault(require("../models/meal"));
const customer_meal_1 = __importDefault(require("../models/customer_meal"));
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
const order_meal_1 = __importDefault(require("../models/order_meal"));
const constants_1 = require("../utils/constants");
const addMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const mealName = req.body.mealName;
        const mealPrice = req.body.mealPrice;
        const description = req.body.description;
        let imageUrl = req.body.imageUrl;
        const discountPercent = req.body.discountPercent;
        const foodCategoryId = req.body.foodCategoryId;
        if (!mealName)
            return res.status(400).json({ message: "Missing mealName" });
        if (!mealPrice)
            return res.status(400).json({ message: "Missing mealPrice" });
        if (!description)
            return res.status(400).json({ message: "Missing description" });
        if (!imageUrl)
            imageUrl = constants_1.defaultMealOfferImageUrl;
        if (discountPercent == null)
            return res.status(400).json({ message: "Missing discountPercent" });
        if (!foodCategoryId)
            return res.status(400).json({ message: "Missing foodCategoryId" });
        const restaurantFoodCategoryId = (_a = (yield typeorm_1.getRepository(restaurant_food_category_1.default).findOne({
            where: {
                foodCategory: { id: foodCategoryId },
                restaurant: { id: req.body.restaurant.id },
            },
        }))) === null || _a === void 0 ? void 0 : _a.id;
        typeorm_1.getRepository(meal_1.default)
            .save({
            mealName: mealName,
            mealPrice: mealPrice,
            description: description,
            imageUrl: imageUrl,
            discountPercent: discountPercent,
            restaurantFoodCategory: { id: restaurantFoodCategoryId },
        })
            .then((result) => {
            return res.status(200).json({ message: "Success" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.addMeal = addMeal;
const editMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        const mealId = req.body.mealId;
        const mealName = req.body.mealName;
        const mealPrice = req.body.mealPrice;
        const description = req.body.description;
        let imageUrl = req.body.imageUrl;
        const discountPercent = req.body.discountPercent;
        const foodCategoryId = req.body.foodCategoryId;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        if (!mealName)
            return res.status(400).json({ message: "Missing mealName" });
        if (!mealPrice)
            return res.status(400).json({ message: "Missing mealPrice" });
        if (!description)
            return res.status(400).json({ message: "Missing description" });
        if (!imageUrl)
            imageUrl = constants_1.defaultMealOfferImageUrl;
        if (discountPercent == null)
            return res.status(400).json({ message: "Missing discountPercent" });
        if (!foodCategoryId)
            return res.status(400).json({ message: "Missing foodCategoryId" });
        const restaurantFoodCategoryId = (_b = (yield typeorm_1.getRepository(restaurant_food_category_1.default).findOne({
            where: {
                foodCategory: { id: foodCategoryId },
                restaurant: { id: req.body.restaurant.id },
            },
        }))) === null || _b === void 0 ? void 0 : _b.id;
        typeorm_1.getRepository(meal_1.default)
            .update(mealId, {
            mealName: mealName,
            mealPrice: mealPrice,
            description: description,
            imageUrl: imageUrl,
            discountPercent: discountPercent,
            restaurantFoodCategory: { id: restaurantFoodCategoryId },
        })
            .then((result) => {
            return res.status(200).json({ message: "Success" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.editMeal = editMeal;
const deleteMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mealId = req.query.mealId;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        const mealRepository = typeorm_1.getRepository(meal_1.default);
        mealRepository
            .findOne(mealId, {
            relations: [
                "restaurantFoodCategory",
                "restaurantFoodCategory.restaurant",
            ],
        })
            .then((meal) => {
            if (meal) {
                if (meal.restaurantFoodCategory.restaurant.id == req.body.restaurant.id) {
                    mealRepository
                        .softDelete(meal)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        return res.status(500).json({ message: error.message });
                    });
                }
                else
                    return res.status(403).json({ message: "Forbidden" });
            }
            else
                return res.status(404).json({ message: "Meal Not Found" });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.deleteMeal = deleteMeal;
const favouriteMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mealId = req.body.mealId;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        const customerMealRepository = typeorm_1.getRepository(customer_meal_1.default);
        customerMealRepository
            .findOne({
            where: {
                customer: { id: req.body.customer.id },
                meal: { id: mealId },
            },
        })
            .then((customerMeal) => {
            if (customerMeal) {
                customerMeal.isFavourite = !customerMeal.isFavourite;
                customerMealRepository
                    .save(customerMeal)
                    .then((result) => {
                    return res.status(200).json({ message: "Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else {
                customerMealRepository
                    .save({
                    customer: { id: req.body.customer.id },
                    meal: { id: mealId },
                    isFavourite: true,
                })
                    .then((result) => {
                    return res.status(200).json({ message: "Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.favouriteMeal = favouriteMeal;
const reviewMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mealId = req.body.mealId;
        const mealRate = req.body.mealRate;
        const comment = req.body.comment;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        if (!mealRate)
            return res.status(400).json({ message: "Missing mealRate" });
        if (!comment)
            return res.status(400).json({ message: "Missing comment" });
        const customerMealRepository = typeorm_1.getRepository(customer_meal_1.default);
        customerMealRepository
            .findOne({
            where: {
                customer: { id: req.body.customer.id },
                meal: { id: mealId },
            },
        })
            .then((customerMeal) => {
            if (customerMeal && customerMeal.mealRate) {
                return res
                    .status(400)
                    .json({ message: "You have been already review this meal" });
            }
            else {
                if (!customerMeal) {
                    customerMealRepository
                        .save({
                        customer: { id: req.body.customer.id },
                        meal: { id: mealId },
                        mealRate: mealRate,
                        comment: comment,
                    })
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        console.log(error);
                        return res.status(500).json({ message: error.message });
                    });
                }
                else {
                    customerMeal.mealRate = mealRate;
                    customerMeal.comment = comment;
                    customerMealRepository
                        .save(customerMeal)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        console.log(error);
                        return res.status(500).json({ message: error.message });
                    });
                }
            }
        })
            .catch((error) => {
            console.log(error);
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ message: error.message });
    }
});
exports.reviewMeal = reviewMeal;
const getMealByID = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mealId = req.query.mealId;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        const mealRepository = typeorm_1.getRepository(meal_1.default);
        mealRepository
            .findOne(mealId, {
            relations: [
                "restaurantFoodCategory",
                "restaurantFoodCategory.foodCategory",
                "restaurantFoodCategory.restaurant",
                "reviews",
                "reviews.customer",
            ],
        })
            .then((meal) => {
            if (meal) {
                meal.reviews = meal.reviews.slice(0, 2);
                let deleteReviews = [];
                const isCustomer = req.body.customer;
                let customerID;
                if (isCustomer)
                    customerID = req.body.customer.id;
                meal.reviews.forEach((customerMeal) => {
                    if (isCustomer && customerID == customerMeal.customer.id)
                        Object.assign(meal, { isFavourite: customerMeal.isFavourite });
                    if (customerMeal.mealRate != null && customerMeal.comment != null) {
                        delete customerMeal.customer.password;
                        delete customerMeal.customer.phoneNumber;
                        delete customerMeal.customer.username;
                    }
                    else
                        deleteReviews.push(customerMeal);
                });
                meal.reviews = meal.reviews.filter((e) => {
                    if (!deleteReviews.find((element) => e.id == element.id))
                        return e;
                });
                return res.status(200).json(meal);
            }
            else
                return res.status(404).json({ message: "Meal Not Found" });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getMealByID = getMealByID;
const searchMeal = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mealName = req.query.mealName;
        if (!mealName)
            return res.status(400).json({ message: "Missing mealName" });
        typeorm_1.getRepository(meal_1.default)
            .createQueryBuilder()
            .select()
            .where("meal_name like :label", { label: `%${mealName}%` })
            .getMany()
            .then((meals) => {
            return res.status(200).json(meals);
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.searchMeal = searchMeal;
const getAllMeals = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    try {
        const page = (_c = req.query.page) !== null && _c !== void 0 ? _c : 1;
        const pageCount = 10;
        let foodCategoriesIDs;
        if (req.body.restaurant) {
            foodCategoriesIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default).find({
                where: {
                    restaurant: { id: req.body.restaurant.id },
                },
            })).map((e) => e.id);
        }
        else {
            const customerFoodCategories = req.body.customer.foodCategories.map((e) => e.id);
            foodCategoriesIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default)
                .createQueryBuilder()
                .select("id")
                .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
                .getRawMany()).map((e) => e.id);
        }
        typeorm_1.getRepository(meal_1.default)
            .createQueryBuilder()
            .select()
            .where("restaurant_food_category_id IN (:ids)", {
            ids: foodCategoriesIDs,
        })
            .skip((page - 1) * pageCount)
            .take(pageCount)
            .getMany()
            .then((meals) => {
            return res.status(200).json(meals);
        })
            .catch((err) => res.status(500).json({ message: err.message }));
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getAllMeals = getAllMeals;
const getAllReviews = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    try {
        const mealId = req.query.mealId;
        const page = (_d = req.query.page) !== null && _d !== void 0 ? _d : 1;
        const pageCount = 15;
        if (!mealId)
            return res.status(400).json({ message: "Missing mealId" });
        typeorm_1.getRepository(customer_meal_1.default)
            .find({
            relations: ["customer"],
            where: {
                meal: { id: mealId },
                mealRate: typeorm_1.Not(typeorm_1.IsNull()),
                comment: typeorm_1.Not(typeorm_1.IsNull()),
            },
            skip: (page - 1) * pageCount,
            take: pageCount,
        })
            .then((reviews) => {
            return res.status(200).json(reviews);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getAllReviews = getAllReviews;
const getMostOrderedMeals = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
        const mealRepository = typeorm_1.getRepository(meal_1.default);
        const orderMealRepository = typeorm_1.getRepository(order_meal_1.default);
        let whereCond;
        let whereValue;
        if (req.body.customer) {
            whereCond = "food_category_id IN (:ids)";
            whereValue = {
                ids: req.body.customer.foodCategories.map((e) => e.id),
            };
        }
        else {
            whereCond = "restaurant_id IN (:id)";
            whereValue = {
                id: req.body.restaurant.id,
            };
        }
        restaurantFoodCategoryRepository
            .createQueryBuilder()
            .where(whereCond, whereValue)
            .distinct(true)
            .getMany()
            .then((restaurantFoodCategories) => {
            if (restaurantFoodCategories.length > 0) {
                mealRepository
                    .createQueryBuilder()
                    .where("restaurant_food_category_id IN (:selectedIDs)", {
                    selectedIDs: restaurantFoodCategories.map((e) => e.id),
                })
                    .getMany()
                    .then((allMeals) => {
                    if (allMeals.length > 0) {
                        orderMealRepository
                            .createQueryBuilder()
                            .select("meal_id as mealId")
                            .addSelect("SUM(item_quantity) AS itemQuantity")
                            .where("meal_id IN (:meals)", {
                            meals: allMeals.map((e) => e.id),
                        })
                            .orderBy("itemQuantity", "DESC")
                            .groupBy("meal_id")
                            .getRawMany()
                            .then((items) => {
                            mealRepository
                                .findByIds(items.map((e) => e.mealId), {
                                take: 3,
                            })
                                .then((meals) => {
                                return res.status(200).json(meals);
                            })
                                .catch((err) => {
                                return res.status(500).json({ message: err.message });
                            });
                        })
                            .catch((error) => {
                            return res.status(500).json({ message: error.message });
                        });
                    }
                    else
                        return res.status(404).json({ message: "No Meals Found" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "No Meals Found" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
        mealRepository.find;
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getMostOrderedMeals = getMostOrderedMeals;
