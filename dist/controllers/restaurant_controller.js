"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRestaurantMostOrderedOffers = exports.getRestaurantMostOrderedMeals = exports.getMostPopularRestaurants = exports.getRestaurantByID = exports.getAllReviews = exports.getAllRestaurants = exports.reviewRestaurant = exports.favouriteRestaurant = exports.searchRestaurant = void 0;
const typeorm_1 = require("typeorm");
const restaurant_1 = __importDefault(require("../models/restaurant"));
const customer_restaurant_1 = __importDefault(require("../models/customer_restaurant"));
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
const meal_1 = __importDefault(require("../models/meal"));
const order_meal_1 = __importDefault(require("../models/order_meal"));
const offer_1 = __importDefault(require("../models/offer"));
const searchRestaurant = (req, res, next) => {
    try {
        const name = req.query.name;
        if (!name)
            return res.status(400).json({ message: "Missing name" });
        typeorm_1.getRepository(restaurant_1.default)
            .find({
            where: {
                restaurantName: typeorm_1.Like(`%${name}%`),
            },
        })
            .then((restaurants) => {
            return res.status(200).json(restaurants);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
};
exports.searchRestaurant = searchRestaurant;
const favouriteRestaurant = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.body.restaurantId;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        const customerRestaurantRepository = typeorm_1.getRepository(customer_restaurant_1.default);
        customerRestaurantRepository
            .findOne({
            where: {
                customer: { id: req.body.customer.id },
                restaurant: { id: restaurantId },
            },
        })
            .then((customerRestaurant) => {
            if (customerRestaurant) {
                customerRestaurant.isFavourite = !customerRestaurant.isFavourite;
                customerRestaurantRepository
                    .save(customerRestaurant)
                    .then((result) => {
                    return res.status(200).json({ message: "Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else {
                customerRestaurantRepository
                    .save({
                    customer: { id: req.body.customer.id },
                    restaurant: { id: restaurantId },
                    isFavourite: true,
                })
                    .then((result) => {
                    return res.status(200).json({ message: "Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.favouriteRestaurant = favouriteRestaurant;
const reviewRestaurant = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.body.restaurantId;
        const restaurantRate = req.body.restaurantRate;
        const comment = req.body.comment;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        if (!restaurantRate)
            return res.status(400).json({ message: "Missing restaurantRate" });
        if (!comment)
            return res.status(400).json({ message: "Missing comment" });
        const customerRestaurantRepository = typeorm_1.getRepository(customer_restaurant_1.default);
        customerRestaurantRepository
            .findOne({
            where: {
                customer: { id: req.body.customer.id },
                restaurant: { id: restaurantId },
            },
        })
            .then((customerRestaurant) => {
            if (customerRestaurant && customerRestaurant.restaurantRate) {
                return res
                    .status(400)
                    .json({ message: "You have been already review this restaurant" });
            }
            else {
                if (!customerRestaurant) {
                    customerRestaurantRepository
                        .save({
                        customer: { id: req.body.customer.id },
                        restaurant: { id: restaurantId },
                        restaurantRate: restaurantRate,
                        comment: comment,
                    })
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        return res.status(500).json({ message: error.message });
                    });
                }
                else {
                    customerRestaurant.restaurantRate = restaurantRate;
                    customerRestaurant.comment = comment;
                    customerRestaurantRepository
                        .save(customerRestaurant)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((error) => {
                        return res.status(500).json({ message: error.message });
                    });
                }
            }
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.reviewRestaurant = reviewRestaurant;
const getAllRestaurants = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const page = (_a = req.query.page) !== null && _a !== void 0 ? _a : 1;
        const pageCount = 10;
        let restaurantsIDs;
        const customerFoodCategories = req.body.customer.foodCategories.map((e) => e.id);
        restaurantsIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default)
            .createQueryBuilder()
            .select("restaurant_id as id")
            .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
            .getRawMany()).map((e) => e.id);
        typeorm_1.getRepository(restaurant_1.default)
            .createQueryBuilder()
            .select()
            .where("id IN (:ids)", {
            ids: restaurantsIDs,
        })
            .skip((page - 1) * pageCount)
            .take(pageCount)
            .getMany()
            .then((restaurants) => {
            return res.status(200).json(restaurants);
        })
            .catch((err) => res.status(500).json({ message: err.message }));
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getAllRestaurants = getAllRestaurants;
const getAllReviews = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        const restaurantId = req.query.restaurantId;
        const page = (_b = req.query.page) !== null && _b !== void 0 ? _b : 1;
        const pageCount = 15;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        typeorm_1.getRepository(customer_restaurant_1.default)
            .find({
            relations: ["customer"],
            where: {
                restaurant: { id: restaurantId },
            },
            skip: (page - 1) * pageCount,
            take: pageCount,
        })
            .then((reviews) => {
            return res.status(200).json(reviews);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getAllReviews = getAllReviews;
const getRestaurantByID = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.query.restaurantId;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        const mealRepository = typeorm_1.getRepository(restaurant_1.default);
        mealRepository
            .findOne(restaurantId, {
            relations: ["contacts", "reviews", "reviews.customer"],
        })
            .then((restaurant) => {
            if (restaurant) {
                restaurant.reviews = restaurant.reviews.slice(0, 2);
                let deleteReviews = [];
                restaurant.reviews.forEach((customerRestaurant) => {
                    if (req.body.customer.id == customerRestaurant.customer.id)
                        Object.assign(restaurant, {
                            isFavourite: customerRestaurant.isFavourite,
                        });
                    if (customerRestaurant.restaurantRate != null &&
                        customerRestaurant.comment != null) {
                        delete customerRestaurant.customer.password;
                        delete customerRestaurant.customer.phoneNumber;
                        delete customerRestaurant.customer.username;
                    }
                    else
                        deleteReviews.push(customerRestaurant);
                });
                restaurant.reviews = restaurant.reviews.filter((e) => {
                    if (!deleteReviews.find((element) => e.id == element.id))
                        return e;
                });
                return res.status(200).json(restaurant);
            }
            else
                return res.status(404).json({ message: "Restaurant Not Found" });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getRestaurantByID = getRestaurantByID;
const getMostPopularRestaurants = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const customerFoodCategories = req.body.customer.foodCategories.map((e) => e.id);
        const restaurantsIDs = (yield typeorm_1.getRepository(restaurant_food_category_1.default)
            .createQueryBuilder()
            .select("restaurant_id")
            .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
            .getRawMany()).map((e) => e.restaurant_id);
        typeorm_1.getRepository(restaurant_1.default)
            .find({
            where: {
                id: typeorm_1.In(restaurantsIDs),
                rating: typeorm_1.MoreThanOrEqual(4),
            },
        })
            .then((restaurants) => {
            return res.status(200).json(restaurants);
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getMostPopularRestaurants = getMostPopularRestaurants;
const getRestaurantMostOrderedMeals = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.query.restaurantId;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
        const mealRepository = typeorm_1.getRepository(meal_1.default);
        const orderMealRepository = typeorm_1.getRepository(order_meal_1.default);
        let whereCond;
        let whereValue;
        whereCond = "restaurant_id = :id";
        whereValue = {
            id: restaurantId,
        };
        restaurantFoodCategoryRepository
            .createQueryBuilder()
            .where(whereCond, whereValue)
            .distinct(true)
            .getMany()
            .then((restaurantFoodCategories) => {
            if (restaurantFoodCategories.length > 0) {
                mealRepository
                    .createQueryBuilder()
                    .where("restaurant_food_category_id IN (:selectedIDs)", {
                    selectedIDs: restaurantFoodCategories.map((e) => e.id),
                })
                    .getMany()
                    .then((allMeals) => {
                    if (allMeals.length > 0) {
                        orderMealRepository
                            .createQueryBuilder()
                            .select("meal_id as mealId")
                            .addSelect("SUM(item_quantity) AS itemQuantity")
                            .where("meal_id IN (:meals)", {
                            meals: allMeals.map((e) => e.id),
                        })
                            .orderBy("itemQuantity", "DESC")
                            .groupBy("meal_id")
                            .getRawMany()
                            .then((items) => {
                            mealRepository
                                .findByIds(items.map((e) => e.mealId), {
                                take: 3,
                            })
                                .then((meals) => {
                                return res.status(200).json(meals);
                            })
                                .catch((err) => {
                                return res.status(500).json({ message: err.message });
                            });
                        })
                            .catch((error) => {
                            return res.status(500).json({ message: error.message });
                        });
                    }
                    else
                        return res.status(404).json({ message: "No Meals Found" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "No Meals Found" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
        mealRepository.find;
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getRestaurantMostOrderedMeals = getRestaurantMostOrderedMeals;
const getRestaurantMostOrderedOffers = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const restaurantId = req.query.restaurantId;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        const offerRepository = typeorm_1.getRepository(offer_1.default);
        const orderMealRepository = typeorm_1.getRepository(order_meal_1.default);
        const whereCond = "restaurant_id = :id";
        let whereValue = { id: restaurantId };
        offerRepository
            .createQueryBuilder()
            .where(whereCond, whereValue)
            .getMany()
            .then((allOffers) => {
            if (allOffers.length > 0) {
                orderMealRepository
                    .createQueryBuilder()
                    .select("offer_id as offerId")
                    .addSelect("SUM(item_quantity) AS itemQuantity")
                    .where("offer_id IN (:offers)", {
                    offers: allOffers.map((e) => e.id),
                })
                    .orderBy("itemQuantity", "DESC")
                    .groupBy("offer_id")
                    .getRawMany()
                    .then((items) => {
                    offerRepository
                        .findByIds(items.map((e) => e.offerId), {
                        take: 3,
                    })
                        .then((offers) => {
                        return res.status(200).json(offers);
                    })
                        .catch((err) => {
                        return res.status(500).json({ message: err.message });
                    });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "No Offers Found" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getRestaurantMostOrderedOffers = getRestaurantMostOrderedOffers;
