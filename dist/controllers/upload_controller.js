"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteImage = exports.uploadController = void 0;
const cloudinary_1 = require("cloudinary");
const streamifier_1 = __importDefault(require("streamifier"));
const destinationFolder = "images";
cloudinary_1.v2.config({
    cloud_name: "smart-restaurants-storage",
    api_key: "283853167933273",
    api_secret: "NWkZKGxZTcZ9Ga_qBXqiIFYVsfY",
});
const uploadController = function (req, res, next) {
    let streamUpload = (req) => {
        return new Promise((resolve, reject) => {
            let stream = cloudinary_1.v2.uploader.upload_stream({
                folder: destinationFolder,
            }, (error, result) => {
                if (result) {
                    resolve(result);
                }
                else {
                    reject(error);
                }
            });
            streamifier_1.default.createReadStream(req.file.buffer).pipe(stream);
        });
    };
    streamUpload(req)
        .then((result) => {
        console.log(result);
        if (result)
            res.status(200).json({
                imageUrl: result["secure_url"],
            });
    })
        .catch((err) => {
        res.status(500).json({ message: "Failed", error: err.message });
    });
};
exports.uploadController = uploadController;
const deleteImage = (imageUrl) => __awaiter(void 0, void 0, void 0, function* () {
    const publicId = `${destinationFolder}/${imageUrl.split("${destinationFolder}/")[1]}`;
    return cloudinary_1.v2.uploader
        .destroy(publicId, function (result) {
        console.log(result);
    })
        .then((result) => {
        if (result)
            return true;
    })
        .catch((err) => {
        return false;
    });
});
exports.deleteImage = deleteImage;
