"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeOrderState = exports.getDeliveringOrders = exports.getProcessingOrders = exports.getPendingOrders = exports.getOrderByID = exports.getFinishedOrders = exports.getUnfinishedOrders = exports.createOrder = void 0;
const typeorm_1 = require("typeorm");
const order_1 = __importDefault(require("../models/order"));
const meal_1 = __importDefault(require("../models/meal"));
const offer_1 = __importDefault(require("../models/offer"));
const constants_1 = require("../utils/constants");
const createOrder = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    try {
        const restaurantId = req.body.restaurantId;
        const customerLat = req.body.customerLat;
        const customerLong = req.body.customerLong;
        const mealsIDs = req.body.meals;
        const offersIDs = req.body.offers;
        if (!restaurantId)
            return res.status(400).json({ message: "Missing restaurantId" });
        if (!mealsIDs && !offersIDs)
            return res.status(400).json({ message: "Missing meals || offers" });
        let meals = [];
        if (mealsIDs)
            meals =
                (_a = (yield typeorm_1.getRepository(meal_1.default).findByIds(mealsIDs.map((e) => e.id)))) !== null && _a !== void 0 ? _a : [];
        let offers = [];
        if (offersIDs)
            offers =
                (_b = (yield typeorm_1.getRepository(offer_1.default).findByIds(offersIDs.map((e) => e.id)))) !== null && _b !== void 0 ? _b : [];
        let orderItems = [];
        for (let i = 0; i < (meals === null || meals === void 0 ? void 0 : meals.length); i++)
            orderItems.push({
                meal: meals[i],
                itemPrice: meals[i].mealPrice,
                itemQuantity: mealsIDs.find((e) => e.id == meals[i].id).quantity,
            });
        for (let i = 0; i < (offers === null || offers === void 0 ? void 0 : offers.length); i++)
            orderItems.push({
                offer: offers[i],
                itemPrice: offers[i].price,
                itemQuantity: offersIDs.find((e) => e.id == offers[i].id).quantity,
            });
        typeorm_1.getRepository(order_1.default)
            .save({
            customer: { id: req.body.customer.id },
            customerLat: customerLat,
            customerLong: customerLong,
            orderDate: new Date(),
            orderTax: 0,
            restaurant: { id: restaurantId },
            orderMeals: orderItems,
            status: { id: constants_1.PENDING_STATE },
        })
            .then((result) => {
            return res.status(200).json({ message: "Success" });
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.createOrder = createOrder;
const getUnfinishedOrders = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        typeorm_1.getRepository(order_1.default)
            .find({
            relations: [
                "orderMeals",
                "orderMeals.meal",
                "orderMeals.offer",
                "restaurant",
                "status",
            ],
            where: {
                customer: { id: req.body.customer.id },
                status: {
                    id: typeorm_1.In([constants_1.PENDING_STATE, constants_1.PROCESSING_STATE, constants_1.DELIVERING_STATE]),
                },
            },
        })
            .then((orders) => {
            return res.status(200).json(orders);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getUnfinishedOrders = getUnfinishedOrders;
const getFinishedOrders = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let whereCond = {
            status: { id: constants_1.FINISHED_STATE },
        };
        if (req.body.customer)
            whereCond["customer"] = { id: req.body.customer.id };
        else
            whereCond["restaurant"] = { id: req.body.restaurant.id };
        typeorm_1.getRepository(order_1.default)
            .find({
            relations: [
                "customer",
                "restaurant",
                "orderMeals",
                "orderMeals.meal",
                "orderMeals.offer",
            ],
            where: whereCond,
        })
            .then((orders) => {
            return res.status(200).json(orders);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getFinishedOrders = getFinishedOrders;
const getOrderByID = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const orderId = req.query.orderId;
        if (!orderId)
            return res.status(400).json({ message: "Missing orderId" });
        typeorm_1.getRepository(order_1.default)
            .findOne(orderId, {
            relations: [
                "orderMeals",
                "orderMeals.meal",
                "orderMeals.offer",
                "customer",
                "restaurant",
            ],
        })
            .then((order) => {
            if (order)
                return res.status(200).json(order);
            else
                return res.status(404).json({ message: "Order Not Found" });
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getOrderByID = getOrderByID;
const getPendingOrders = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        typeorm_1.getRepository(order_1.default)
            .find({
            relations: ["customer"],
            where: {
                restaurant: { id: req.body.restaurant.id },
                status: { id: constants_1.PENDING_STATE },
            },
        })
            .then((orders) => {
            return res.status(200).json(orders);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getPendingOrders = getPendingOrders;
const getProcessingOrders = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        typeorm_1.getRepository(order_1.default)
            .find({
            relations: ["customer"],
            where: {
                restaurant: { id: req.body.restaurant.id },
                status: { id: constants_1.PROCESSING_STATE },
            },
        })
            .then((orders) => {
            return res.status(200).json(orders);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getProcessingOrders = getProcessingOrders;
const getDeliveringOrders = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        typeorm_1.getRepository(order_1.default)
            .find({
            relations: ["customer"],
            where: {
                restaurant: { id: req.body.restaurant.id },
                status: { id: constants_1.DELIVERING_STATE },
            },
        })
            .then((orders) => {
            return res.status(200).json(orders);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.getDeliveringOrders = getDeliveringOrders;
const changeOrderState = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const orderId = req.body.orderId;
        const state = req.body.state;
        if (!orderId)
            return res.status(400).json({ message: "Missing orderId" });
        if (!state)
            return res.status(400).json({ message: "Missing state" });
        const orderRepository = typeorm_1.getRepository(order_1.default);
        orderRepository
            .findOne(orderId, {
            where: {
                restaurant: { id: req.body.restaurant.id },
            },
        })
            .then((order) => {
            if (order) {
                order.status = { id: state };
                orderRepository
                    .save(order)
                    .then((result) => {
                    return res.status(200).json({ message: " Success" });
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
            else
                return res.status(404).json({ message: "Order Not Found" });
        });
    }
    catch (err) {
        return res.status(500).json({ message: err.message });
    }
});
exports.changeOrderState = changeOrderState;
