"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.editRestaurantFoodCategories = exports.editRestaurantProfile = exports.editRestaurantContacts = exports.editCustomerProfile = exports.changePassword = exports.editCustomerFoodCategories = exports.getFavouriteRestaurants = exports.getFavouiteMeals = void 0;
const typeorm_1 = require("typeorm");
const customer_meal_1 = __importDefault(require("../models/customer_meal"));
const customer_restaurant_1 = __importDefault(require("../models/customer_restaurant"));
const customer_1 = __importDefault(require("../models/customer"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const contact_1 = __importDefault(require("../models/contact"));
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
const getFavouiteMeals = (req, res, next) => {
    try {
        typeorm_1.getRepository(customer_meal_1.default)
            .find({
            relations: ["meal"],
            where: {
                customer: { id: req.body.customer.id },
                isFavourite: true,
            },
        })
            .then((meals) => {
            return res.status(200).json(meals);
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
};
exports.getFavouiteMeals = getFavouiteMeals;
const getFavouriteRestaurants = (req, res, next) => {
    try {
        typeorm_1.getRepository(customer_restaurant_1.default)
            .find({
            relations: ["restaurant"],
            where: {
                customer: { id: req.body.customer.id },
                isFavourite: true,
            },
        })
            .then((restaurants) => {
            return res.status(200).json(restaurants);
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (err) {
        return res.status(500).json({ message: err.message });
    }
};
exports.getFavouriteRestaurants = getFavouriteRestaurants;
const editCustomerFoodCategories = (req, res, next) => {
    try {
        const foodCategories = req.body.foodCategories;
        if (!foodCategories)
            return res.status(400).json({ message: "Missing foodCategories" });
        const finalCustomer = req.body.customer;
        finalCustomer.foodCategories = foodCategories;
        typeorm_1.getRepository(customer_1.default)
            .save(finalCustomer)
            .then((result) => {
            return res.status(200).json({ message: "Success" });
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (err) {
        return res.status(500).json({ message: err.message });
    }
};
exports.editCustomerFoodCategories = editCustomerFoodCategories;
const changePassword = (req, res, next) => {
    try {
        const oldPassword = req.body.oldPassword;
        const newPassword = req.body.newPassword;
        if (req.body.customer) {
            bcrypt_1.default
                .compare(oldPassword, req.body.customer.password)
                .then((isEqual) => {
                if (!isEqual) {
                    return res.status(422).json({ message: "Failed" });
                }
                bcrypt_1.default.hash(newPassword, 12, (err, hasedPassword) => {
                    req.body.customer.password = hasedPassword;
                    typeorm_1.getRepository(customer_1.default)
                        .save(req.body.customer)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((err) => {
                        return res.status(500).json({ message: err.message });
                    });
                });
            })
                .catch((err) => res.status(500).json({ message: "Failed", error: err.message }));
        }
        else {
            bcrypt_1.default
                .compare(oldPassword, req.body.restaurant.password)
                .then((isEqual) => {
                if (!isEqual) {
                    return res.status(422).json({ message: "Failed" });
                }
                bcrypt_1.default.hash(newPassword, 12, (err, hasedPassword) => {
                    req.body.restaurant.password = hasedPassword;
                    typeorm_1.getRepository(restaurant_1.default)
                        .save(req.body.restaurant)
                        .then((result) => {
                        return res.status(200).json({ message: "Success" });
                    })
                        .catch((err) => {
                        return res.status(500).json({ message: err.message });
                    });
                });
            })
                .catch((err) => res.status(500).json({ message: "Failed", error: err.message }));
        }
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
};
exports.changePassword = changePassword;
const editCustomerProfile = (req, res, next) => {
    try {
        const username = req.body.username;
        const firstName = req.body.firstName;
        const lastName = req.body.lastName;
        const phoneNumber = req.body.phoneNumber;
        if (!username)
            return res.status(400).json({ message: "Missing username" });
        if (!firstName)
            return res.status(400).json({ message: "Missing firstName" });
        if (!lastName)
            return res.status(400).json({ message: "Missing lastName" });
        if (!phoneNumber)
            return res.status(400).json({ message: "Missing phoneNumber" });
        const customerRepository = typeorm_1.getRepository(customer_1.default);
        customerRepository
            .findOne({
            where: {
                username: username,
                id: typeorm_1.Not(req.body.customer.id),
            },
        })
            .then((result) => {
            if (result) {
                return res
                    .status(400)
                    .json({ message: "This username is already used" });
            }
            else {
                typeorm_1.getRepository(restaurant_1.default)
                    .findOne({
                    where: {
                        username: username,
                    },
                })
                    .then((result) => {
                    if (result) {
                        return res
                            .status(400)
                            .json({ message: "This username is already used" });
                    }
                    else {
                        req.body.customer.username = username;
                        req.body.customer.firstName = firstName;
                        req.body.customer.lastName = lastName;
                        req.body.customer.phoneNumber = phoneNumber;
                        customerRepository
                            .save(req.body.customer)
                            .then((result) => {
                            return res.status(200).json({ message: "Success" });
                        })
                            .catch((error) => {
                            return res.status(500).json({ message: error.message });
                        });
                    }
                });
            }
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (err) {
        return res.status(500).json({ message: err.message });
    }
};
exports.editCustomerProfile = editCustomerProfile;
const editRestaurantContacts = (req, res, next) => {
    try {
        const contacts = req.body.contacts;
        if (!contacts)
            return res.status(400).json({ message: "Missing contacts" });
        req.body.restaurant.contacts = contacts;
        typeorm_1.getRepository(restaurant_1.default)
            .save(req.body.restaurant)
            .then((result) => {
            typeorm_1.getRepository(contact_1.default).delete({
                restaurant: { id: typeorm_1.IsNull() },
            });
            return res.status(200).json({ message: "Success" });
        })
            .catch((err) => {
            return res.status(500).json({ message: err.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
};
exports.editRestaurantContacts = editRestaurantContacts;
const editRestaurantProfile = (req, res, next) => {
    try {
        const username = req.body.username;
        const restaurantName = req.body.restaurantName;
        const address = req.body.address;
        const latitude = req.body.latitude;
        const longitude = req.body.longitude;
        const hasTables = req.body.hasTables;
        const hasDelivery = req.body.hasDelivery;
        if (!username)
            return res.status(400).json({ message: "Missing username" });
        if (!restaurantName)
            return res.status(400).json({ message: "Missing restaurantName" });
        if (!address)
            return res.status(400).json({ message: "Missing address" });
        if (!latitude)
            return res.status(400).json({ message: "Missing latitude" });
        if (!longitude)
            return res.status(400).json({ message: "Missing longitude" });
        if (hasTables == null)
            return res.status(400).json({ message: "Missing hasTables" });
        if (hasDelivery == null)
            return res.status(400).json({ message: "Missing hasDelivery" });
        const restaurantRepository = typeorm_1.getRepository(restaurant_1.default);
        restaurantRepository
            .findOne({
            where: {
                username: username,
                id: typeorm_1.Not(req.body.restaurant.id),
            },
        })
            .then((result) => {
            if (result) {
                return res
                    .status(400)
                    .json({ message: "This username is already used" });
            }
            else {
                typeorm_1.getRepository(customer_1.default)
                    .findOne({
                    where: {
                        username: username,
                    },
                })
                    .then((result) => {
                    if (result) {
                        return res
                            .status(400)
                            .json({ message: "This username is already used" });
                    }
                    else {
                        req.body.restaurant.username = username;
                        req.body.restaurant.restaurantName = restaurantName;
                        req.body.restaurant.address = address;
                        req.body.restaurant.latitude = latitude;
                        req.body.restaurant.longitude = longitude;
                        req.body.restaurant.hasTables = hasTables;
                        req.body.restaurant.hasDelivery = hasDelivery;
                        restaurantRepository
                            .save(req.body.restaurant)
                            .then((result) => {
                            return res.status(200).json({ message: "Success" });
                        })
                            .catch((error) => {
                            return res.status(500).json({ message: error.message });
                        });
                    }
                })
                    .catch((error) => {
                    return res.status(500).json({ message: error.message });
                });
            }
        })
            .catch((error) => {
            return res.status(500).json({ message: error.message });
        });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
};
exports.editRestaurantProfile = editRestaurantProfile;
const editRestaurantFoodCategories = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const addedFoodCategories = req.body.addedFoodCategories;
        const deletedFoodCategories = req.body.deletedFoodCategories;
        if (addedFoodCategories.length == 0 && deletedFoodCategories.length == 0)
            return res.status(400).json({
                message: "Missing addedFoodCategories || deletedFoodCategories",
            });
        const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
        const foodCategories = yield restaurantFoodCategoryRepository.find({
            relations: ["meals"],
            where: {
                restaurant: { id: req.body.restaurant.id },
                foodCategory: { id: typeorm_1.In(deletedFoodCategories.map((e) => e.id)) },
            },
        });
        for (let i = 0; i < foodCategories.length; i++) {
            if (foodCategories[i].meals.length > 0)
                return res.status(400).json({
                    message: `You Should Delete All Meals Of Food Category ID ${foodCategories[i].id}`,
                });
        }
        if (foodCategories.length > 0)
            yield restaurantFoodCategoryRepository.delete(foodCategories.map((e) => e.id));
        let wantToAddFoodCategories = [];
        for (let i = 0; i < (addedFoodCategories === null || addedFoodCategories === void 0 ? void 0 : addedFoodCategories.length); i++) {
            wantToAddFoodCategories.push({
                restaurant: { id: req.body.restaurant.id },
                foodCategory: addedFoodCategories[i],
            });
        }
        if (wantToAddFoodCategories.length > 0)
            yield restaurantFoodCategoryRepository.save(wantToAddFoodCategories);
        return res.status(200).json({ message: "Success" });
    }
    catch (error) {
        return res.status(500).json({ message: error.message });
    }
});
exports.editRestaurantFoodCategories = editRestaurantFoodCategories;
