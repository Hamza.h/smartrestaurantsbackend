"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.signUpCustomer = exports.signUpRestaurant = exports.auth = void 0;
const food_category_1 = __importDefault(require("../models/food_category"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const customer_1 = __importDefault(require("../models/customer"));
const contact_1 = __importDefault(require("../models/contact"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const express_validator_1 = require("express-validator");
const restaurant_food_category_1 = __importDefault(require("../models/restaurant_food_category"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const restaurant_repository_1 = require("../repositories/restaurant_repository");
const constants_1 = require("../utils/constants");
const typeorm_1 = require("typeorm");
const auth = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                message: "Failed",
                error: errors.array()[0].msg,
            });
        }
        const username = req.body.username;
        const password = req.body.password;
        let loadedUser;
        let isCustomer = true;
        const customerRepository = typeorm_1.getRepository(customer_1.default);
        loadedUser = yield customerRepository.findOne({
            where: { username: username },
            relations: ["foodCategories"],
        });
        if (!loadedUser) {
            isCustomer = false;
            const restaurantRepository = typeorm_1.getCustomRepository(restaurant_repository_1.RestaurantRepository);
            loadedUser = yield restaurantRepository.findOne({
                where: { username: username },
                relations: ["contacts"],
            });
            yield restaurantRepository
                .getAllFoodCategories(loadedUser)
                .then((foodCategories) => {
                Object.assign(loadedUser, { foodCategories: foodCategories });
            })
                .catch((error) => {
                res.status(500).json({ message: "Failed", error: error.message });
            });
        }
        if (!loadedUser) {
            return res.status(404).json({ message: "Something Went Wrong" });
        }
        console.log(loadedUser);
        bcrypt_1.default
            .compare(password, loadedUser.password)
            .then((isEqual) => {
            if (!isEqual) {
                return res.status(422).json({ message: "Failed" });
            }
            const token = jsonwebtoken_1.default.sign({
                username: loadedUser === null || loadedUser === void 0 ? void 0 : loadedUser.username,
                isCustomer: isCustomer,
                date: Date.now(),
            }, constants_1.encryptSecretKey, { expiresIn: "7d" });
            Object.assign(loadedUser, { jwtToken: token });
            delete loadedUser["password"];
            res.status(200).json({
                user: loadedUser,
                isCustomer: isCustomer,
            });
        })
            .catch((err) => res.status(500).json({ message: "Failed", error: err.message }));
    }
    catch (err) {
        res.status(500).json({ message: "Failed", error: err.message });
    }
});
exports.auth = auth;
const signUpRestaurant = (req, res, next) => {
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            message: "Failed",
            errors: errors.array().map((err) => err),
        });
    }
    const restaurantRepository = typeorm_1.getCustomRepository(restaurant_repository_1.RestaurantRepository);
    const restaurantFoodCategoryRepository = typeorm_1.getRepository(restaurant_food_category_1.default);
    const foodCategoryRepository = typeorm_1.getRepository(food_category_1.default);
    bcrypt_1.default.hash(req.body.restaurant.password, 12, (err, hasedPassword) => {
        const restaurant = new restaurant_1.default();
        restaurant.hasDelivery = req.body.restaurant.hasDelivery;
        restaurant.hasTables = req.body.restaurant.hasTables;
        restaurant.restaurantName = req.body.restaurant.restaurantName;
        restaurant.address = req.body.restaurant.address;
        restaurant.latitude = req.body.restaurant.latitude;
        restaurant.longitude = req.body.restaurant.longitude;
        restaurant.username = req.body.restaurant.username;
        restaurant.password = hasedPassword;
        const contact = new contact_1.default();
        contact.label = req.body.contact.label;
        contact.phoneNumber = req.body.contact.phoneNumber;
        restaurant.contacts = [contact];
        restaurantRepository
            .save(restaurant)
            .then((savedRestaurant) => {
            return restaurantRepository
                .addFoodCategories(req.body.foodCategories, savedRestaurant)
                .then((result) => {
                return res.status(200).json({ message: "Success" });
            })
                .catch((error) => {
                res.status(500).json({ message: "Failed" });
            });
        })
            .catch((err) => {
            res.status(500).json({ message: "Failed" });
        });
    });
};
exports.signUpRestaurant = signUpRestaurant;
const signUpCustomer = (req, res, next) => {
    const errors = express_validator_1.validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            message: "Failed",
            errors: errors.array().map((err) => err),
        });
    }
    const foodCategoryRepository = typeorm_1.getRepository(food_category_1.default);
    const customerRepository = typeorm_1.getRepository(customer_1.default);
    bcrypt_1.default.hash(req.body.password, 12, (err, hasedPassword) => __awaiter(void 0, void 0, void 0, function* () {
        const customer = new customer_1.default();
        customer.firstName = req.body.firstName;
        customer.lastName = req.body.lastName;
        customer.phoneNumber = req.body.phoneNumber;
        customer.username = req.body.username;
        customer.password = hasedPassword;
        yield foodCategoryRepository
            .findByIds(req.body.foodCategories)
            .then((foodCategories) => {
            customer.foodCategories = foodCategories;
        });
        customerRepository
            .save(customer)
            .then((savedCustomer) => {
            if (savedCustomer)
                res.status(200).json({
                    message: "Success",
                });
        })
            .catch((err) => {
            res.status(500).json({ message: "Failed" });
        });
    }));
};
exports.signUpCustomer = signUpCustomer;
