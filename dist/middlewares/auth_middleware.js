"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAuth = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const typeorm_1 = require("typeorm");
const customer_1 = __importDefault(require("../models/customer"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const constants_1 = require("../utils/constants");
const isAuth = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    const token = (_b = (_a = req.get("Authorization")) === null || _a === void 0 ? void 0 : _a.split(" ")[1]) !== null && _b !== void 0 ? _b : "";
    if (token === "")
        return res
            .status(400)
            .json({ message: "Missing bearer token in request header" });
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, constants_1.encryptSecretKey);
    }
    catch (err) {
        return res.status(401).json({ message: "Unauthorized" });
    }
    if (!decodedToken)
        return res.status(401).json({ message: "Unauthorized" });
    const isCustomer = decodedToken.isCustomer;
    if (isCustomer) {
        const customer = yield typeorm_1.getRepository(customer_1.default).findOne({
            relations: ["foodCategories"],
            where: { username: decodedToken.username },
        });
        if (!customer)
            return res.status(404).json({ message: "Not Found" });
        Object.assign(req.body, { customer: customer });
    }
    else {
        const restaurant = yield typeorm_1.getRepository(restaurant_1.default).findOne({
            where: { username: decodedToken.username },
        });
        if (!restaurant)
            return res.status(404).json({ message: "Not Found" });
        Object.assign(req.body, { restaurant: restaurant });
    }
    next();
});
exports.isAuth = isAuth;
