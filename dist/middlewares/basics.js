"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CROS_MIDDLEWARE = void 0;
const express_1 = require("express");
const multer_1 = __importDefault(require("multer"));
const uploader = multer_1.default();
const middlewares = [express_1.json(), uploader.none()];
const CROS_MIDDLEWARE = (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    console.log("Allow CROS");
    next();
};
exports.CROS_MIDDLEWARE = CROS_MIDDLEWARE;
exports.default = middlewares;
