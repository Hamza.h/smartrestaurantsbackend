"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_food_category_1 = __importDefault(require("./restaurant_food_category"));
let FoodCategory = class FoodCategory {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], FoodCategory.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "category_name" }),
    __metadata("design:type", String)
], FoodCategory.prototype, "categoryName", void 0);
__decorate([
    typeorm_1.Column({ name: "image_url" }),
    __metadata("design:type", String)
], FoodCategory.prototype, "imageUrl", void 0);
__decorate([
    typeorm_1.Column({ name: "category_color" }),
    __metadata("design:type", String)
], FoodCategory.prototype, "categoryColor", void 0);
__decorate([
    typeorm_1.OneToMany(() => restaurant_food_category_1.default, restaurantFoodCategory => restaurantFoodCategory.foodCategory),
    __metadata("design:type", Array)
], FoodCategory.prototype, "restaurantFoodCategories", void 0);
FoodCategory = __decorate([
    typeorm_1.Entity({ name: "food_category" })
], FoodCategory);
exports.default = FoodCategory;
