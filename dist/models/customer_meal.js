"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const customer_1 = __importDefault(require("./customer"));
const meal_1 = __importDefault(require("./meal"));
let CustomerMeal = class CustomerMeal {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ type: "int" }),
    __metadata("design:type", Number)
], CustomerMeal.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: "int", name: "meal_rate", nullable: true }),
    __metadata("design:type", Number)
], CustomerMeal.prototype, "mealRate", void 0);
__decorate([
    typeorm_1.Column({ type: "bool", name: "is_favourite", default: false }),
    __metadata("design:type", Boolean)
], CustomerMeal.prototype, "isFavourite", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], CustomerMeal.prototype, "comment", void 0);
__decorate([
    typeorm_1.ManyToOne(() => customer_1.default, (customer) => customer.customerMeals),
    typeorm_1.JoinColumn({ name: "customer_id", referencedColumnName: "id" }),
    __metadata("design:type", customer_1.default)
], CustomerMeal.prototype, "customer", void 0);
__decorate([
    typeorm_1.ManyToOne(() => meal_1.default, (meal) => meal.reviews),
    typeorm_1.JoinColumn({ name: "meal_id", referencedColumnName: "id" }),
    __metadata("design:type", meal_1.default)
], CustomerMeal.prototype, "meal", void 0);
CustomerMeal = __decorate([
    typeorm_1.Entity({ name: "customer_meal" })
], CustomerMeal);
exports.default = CustomerMeal;
