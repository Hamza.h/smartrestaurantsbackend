"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_1 = __importDefault(require("./restaurant"));
const order_meal_1 = __importDefault(require("./order_meal"));
const meal_1 = __importDefault(require("./meal"));
const constants_1 = require("../utils/constants");
let Offer = class Offer {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Offer.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "offer_name" }),
    __metadata("design:type", String)
], Offer.prototype, "offerName", void 0);
__decorate([
    typeorm_1.Column({
        name: "start_date",
        default: () => "CURRENT_TIMESTAMP",
        type: "timestamp",
    }),
    __metadata("design:type", Date)
], Offer.prototype, "startDate", void 0);
__decorate([
    typeorm_1.Column({ name: "end_date" }),
    __metadata("design:type", Date)
], Offer.prototype, "endDate", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Offer.prototype, "description", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Offer.prototype, "price", void 0);
__decorate([
    typeorm_1.Column({
        name: "image_url",
        nullable: true,
        default: constants_1.defaultMealOfferImageUrl,
    }),
    __metadata("design:type", String)
], Offer.prototype, "imageUrl", void 0);
__decorate([
    typeorm_1.DeleteDateColumn(),
    __metadata("design:type", Date)
], Offer.prototype, "deletedAt", void 0);
__decorate([
    typeorm_1.ManyToOne(() => restaurant_1.default, (restaurant) => restaurant.offers),
    typeorm_1.JoinColumn({ name: "restaurant_id", referencedColumnName: "id" }),
    __metadata("design:type", restaurant_1.default)
], Offer.prototype, "restaurant", void 0);
__decorate([
    typeorm_1.OneToMany(() => order_meal_1.default, (orderMeal) => orderMeal.offer),
    __metadata("design:type", Array)
], Offer.prototype, "offerOrders", void 0);
__decorate([
    typeorm_1.ManyToMany(() => meal_1.default, (meal) => meal.offers),
    typeorm_1.JoinTable({
        name: "offer_meal",
        joinColumn: {
            name: "offer_id",
            referencedColumnName: "id",
        },
        inverseJoinColumn: {
            name: "meal_id",
            referencedColumnName: "id",
        },
    }),
    __metadata("design:type", Array)
], Offer.prototype, "meals", void 0);
Offer = __decorate([
    typeorm_1.Entity({ name: "offer" })
], Offer);
exports.default = Offer;
