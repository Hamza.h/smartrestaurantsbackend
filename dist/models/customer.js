"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const food_category_1 = __importDefault(require("./food_category"));
const customer_restaurant_1 = __importDefault(require("./customer_restaurant"));
const order_1 = __importDefault(require("./order"));
const customer_meal_1 = __importDefault(require("./customer_meal"));
const constants_1 = require("../utils/constants");
let Customer = class Customer {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Customer.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "first_name" }),
    __metadata("design:type", String)
], Customer.prototype, "firstName", void 0);
__decorate([
    typeorm_1.Column({ name: "last_name" }),
    __metadata("design:type", String)
], Customer.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column({ name: "phone_number" }),
    __metadata("design:type", String)
], Customer.prototype, "phoneNumber", void 0);
__decorate([
    typeorm_1.Column({ unique: true }),
    __metadata("design:type", String)
], Customer.prototype, "username", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Customer.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ name: "image_url", default: constants_1.defaultCustomerImageUrl }),
    __metadata("design:type", String)
], Customer.prototype, "imageUrl", void 0);
__decorate([
    typeorm_1.ManyToMany(() => food_category_1.default, {
        cascade: true,
    }),
    typeorm_1.JoinTable({
        name: "customer_food_category",
        joinColumn: {
            name: "customer_id",
            referencedColumnName: "id",
        },
        inverseJoinColumn: {
            name: "food_category_id",
            referencedColumnName: "id",
        },
    }),
    __metadata("design:type", Array)
], Customer.prototype, "foodCategories", void 0);
__decorate([
    typeorm_1.OneToMany(() => customer_restaurant_1.default, (customerRestaurant) => customerRestaurant.customer),
    __metadata("design:type", Array)
], Customer.prototype, "customerRestaurants", void 0);
__decorate([
    typeorm_1.OneToMany(() => order_1.default, (order) => order.customer),
    __metadata("design:type", Array)
], Customer.prototype, "orders", void 0);
__decorate([
    typeorm_1.OneToMany(() => customer_meal_1.default, (customerMeal) => customerMeal.customer),
    __metadata("design:type", Array)
], Customer.prototype, "customerMeals", void 0);
Customer = __decorate([
    typeorm_1.Entity({ name: "customer" })
], Customer);
exports.default = Customer;
