"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_1 = __importDefault(require("./restaurant"));
const customer_1 = __importDefault(require("./customer"));
const order_status_1 = __importDefault(require("./order_status"));
const order_meal_1 = __importDefault(require("./order_meal"));
let Order = class Order {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Order.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        name: "order_date",
        default: () => "CURRENT_TIMESTAMP",
        type: "timestamp",
    }),
    __metadata("design:type", Date)
], Order.prototype, "orderDate", void 0);
__decorate([
    typeorm_1.Column({ name: "order_tax", default: 0.0 }),
    __metadata("design:type", Number)
], Order.prototype, "orderTax", void 0);
__decorate([
    typeorm_1.Column({ name: "customer_lat", nullable: true }),
    __metadata("design:type", Number)
], Order.prototype, "customerLat", void 0);
__decorate([
    typeorm_1.Column({ name: "customer_long", nullable: true }),
    __metadata("design:type", Number)
], Order.prototype, "customerLong", void 0);
__decorate([
    typeorm_1.ManyToOne(() => restaurant_1.default, (restaurant) => restaurant.orders),
    typeorm_1.JoinColumn({ name: "restaurant_id", referencedColumnName: "id" }),
    __metadata("design:type", restaurant_1.default)
], Order.prototype, "restaurant", void 0);
__decorate([
    typeorm_1.ManyToOne(() => customer_1.default, (customer) => customer.orders),
    typeorm_1.JoinColumn({ name: "customer_id", referencedColumnName: "id" }),
    __metadata("design:type", customer_1.default)
], Order.prototype, "customer", void 0);
__decorate([
    typeorm_1.ManyToOne(() => order_status_1.default, (orderStatus) => orderStatus.orders),
    typeorm_1.JoinColumn({ name: "status_id", referencedColumnName: "id" }),
    __metadata("design:type", order_status_1.default)
], Order.prototype, "status", void 0);
__decorate([
    typeorm_1.OneToMany(() => order_meal_1.default, (orderMeal) => orderMeal.order, {
        cascade: true,
    }),
    __metadata("design:type", Array)
], Order.prototype, "orderMeals", void 0);
Order = __decorate([
    typeorm_1.Entity({ name: "order" })
], Order);
exports.default = Order;
