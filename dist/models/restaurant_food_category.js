"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_1 = __importDefault(require("./restaurant"));
const food_category_1 = __importDefault(require("./food_category"));
const meal_1 = __importDefault(require("./meal"));
let RestaurantFoodCategory = class RestaurantFoodCategory {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], RestaurantFoodCategory.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => restaurant_1.default, (restaurant) => restaurant.restaurantFoodCategories),
    typeorm_1.JoinColumn({ name: "restaurant_id", referencedColumnName: "id" }),
    __metadata("design:type", restaurant_1.default)
], RestaurantFoodCategory.prototype, "restaurant", void 0);
__decorate([
    typeorm_1.ManyToOne(() => food_category_1.default, (foodCategory) => foodCategory.restaurantFoodCategories),
    typeorm_1.JoinColumn({ name: "food_category_id", referencedColumnName: "id" }),
    __metadata("design:type", food_category_1.default)
], RestaurantFoodCategory.prototype, "foodCategory", void 0);
__decorate([
    typeorm_1.OneToMany(() => meal_1.default, (meal) => meal.restaurantFoodCategory),
    __metadata("design:type", Array)
], RestaurantFoodCategory.prototype, "meals", void 0);
RestaurantFoodCategory = __decorate([
    typeorm_1.Entity({ name: "restaurant_food_category" })
], RestaurantFoodCategory);
exports.default = RestaurantFoodCategory;
