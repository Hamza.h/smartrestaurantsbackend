"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_food_category_1 = __importDefault(require("./restaurant_food_category"));
const customer_restaurant_1 = __importDefault(require("./customer_restaurant"));
const contact_1 = __importDefault(require("./contact"));
const offer_1 = __importDefault(require("./offer"));
const order_1 = __importDefault(require("./order"));
const constants_1 = require("../utils/constants");
let Restaurant = class Restaurant {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Restaurant.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "restaurant_name" }),
    __metadata("design:type", String)
], Restaurant.prototype, "restaurantName", void 0);
__decorate([
    typeorm_1.Column({ name: "lat", type: "double" }),
    __metadata("design:type", Number)
], Restaurant.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({ name: "long", type: "double" }),
    __metadata("design:type", Number)
], Restaurant.prototype, "longitude", void 0);
__decorate([
    typeorm_1.Column({ name: "address" }),
    __metadata("design:type", String)
], Restaurant.prototype, "address", void 0);
__decorate([
    typeorm_1.Column({ name: "has_delivery", default: false }),
    __metadata("design:type", Boolean)
], Restaurant.prototype, "hasDelivery", void 0);
__decorate([
    typeorm_1.Column({ name: "has_tables", default: false }),
    __metadata("design:type", Boolean)
], Restaurant.prototype, "hasTables", void 0);
__decorate([
    typeorm_1.Column({ unique: true }),
    __metadata("design:type", String)
], Restaurant.prototype, "username", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Restaurant.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({
        name: "image_url",
        nullable: true,
        default: constants_1.defaultRestaurantImageUrl,
    }),
    __metadata("design:type", String)
], Restaurant.prototype, "imageUrl", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Restaurant.prototype, "rating", void 0);
__decorate([
    typeorm_1.Column({ name: "order_tax", default: 0.0 }),
    __metadata("design:type", Number)
], Restaurant.prototype, "orderTax", void 0);
__decorate([
    typeorm_1.OneToMany(() => restaurant_food_category_1.default, (restaurantFoodCategory) => restaurantFoodCategory.restaurant),
    __metadata("design:type", Array)
], Restaurant.prototype, "restaurantFoodCategories", void 0);
__decorate([
    typeorm_1.OneToMany(() => customer_restaurant_1.default, (customerRestaurant) => customerRestaurant.restaurant),
    __metadata("design:type", Array)
], Restaurant.prototype, "reviews", void 0);
__decorate([
    typeorm_1.OneToMany(() => contact_1.default, (contact) => contact.restaurant, { cascade: true }),
    __metadata("design:type", Array)
], Restaurant.prototype, "contacts", void 0);
__decorate([
    typeorm_1.OneToMany(() => offer_1.default, (offer) => offer.restaurant),
    __metadata("design:type", Array)
], Restaurant.prototype, "offers", void 0);
__decorate([
    typeorm_1.OneToMany(() => order_1.default, (order) => order.restaurant),
    __metadata("design:type", Array)
], Restaurant.prototype, "orders", void 0);
Restaurant = __decorate([
    typeorm_1.Entity({ name: "restaurant" })
], Restaurant);
exports.default = Restaurant;
