"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const order_1 = __importDefault(require("./order"));
const meal_1 = __importDefault(require("./meal"));
const offer_1 = __importDefault(require("./offer"));
let OrderMeal = class OrderMeal {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], OrderMeal.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "item_price" }),
    __metadata("design:type", Number)
], OrderMeal.prototype, "itemPrice", void 0);
__decorate([
    typeorm_1.Column({ name: "item_quantity", type: "int" }),
    __metadata("design:type", Number)
], OrderMeal.prototype, "itemQuantity", void 0);
__decorate([
    typeorm_1.ManyToOne(() => order_1.default, order => order.orderMeals),
    typeorm_1.JoinColumn({ name: "order_id", referencedColumnName: "id" }),
    __metadata("design:type", order_1.default)
], OrderMeal.prototype, "order", void 0);
__decorate([
    typeorm_1.ManyToOne(() => meal_1.default, meal => meal.mealOrders),
    typeorm_1.JoinColumn({ name: "meal_id", referencedColumnName: "id" }),
    __metadata("design:type", meal_1.default)
], OrderMeal.prototype, "meal", void 0);
__decorate([
    typeorm_1.ManyToOne(() => offer_1.default, offer => offer.offerOrders),
    typeorm_1.JoinColumn({ name: "offer_id", referencedColumnName: "id" }),
    __metadata("design:type", offer_1.default)
], OrderMeal.prototype, "offer", void 0);
OrderMeal = __decorate([
    typeorm_1.Entity({ name: "order_meal" })
], OrderMeal);
exports.default = OrderMeal;
