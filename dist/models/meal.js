"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const restaurant_food_category_1 = __importDefault(require("./restaurant_food_category"));
const customer_meal_1 = __importDefault(require("./customer_meal"));
const order_meal_1 = __importDefault(require("./order_meal"));
const offer_1 = __importDefault(require("./offer"));
const constants_1 = require("../utils/constants");
let Meal = class Meal {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Meal.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: "meal_name" }),
    __metadata("design:type", String)
], Meal.prototype, "mealName", void 0);
__decorate([
    typeorm_1.Column({ name: "meal_price" }),
    __metadata("design:type", Number)
], Meal.prototype, "mealPrice", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Meal.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
        name: "image_url",
        default: constants_1.defaultMealOfferImageUrl,
    }),
    __metadata("design:type", String)
], Meal.prototype, "imageUrl", void 0);
__decorate([
    typeorm_1.Column({ name: "discount_percent", default: 0.0 }),
    __metadata("design:type", Number)
], Meal.prototype, "discountPercent", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Meal.prototype, "rating", void 0);
__decorate([
    typeorm_1.DeleteDateColumn(),
    __metadata("design:type", Date)
], Meal.prototype, "deletedAt", void 0);
__decorate([
    typeorm_1.ManyToOne(() => restaurant_food_category_1.default, (restaurantFoodCategory) => restaurantFoodCategory.meals),
    typeorm_1.JoinColumn({
        name: "restaurant_food_category_id",
        referencedColumnName: "id",
    }),
    __metadata("design:type", restaurant_food_category_1.default)
], Meal.prototype, "restaurantFoodCategory", void 0);
__decorate([
    typeorm_1.OneToMany(() => customer_meal_1.default, (customerMeal) => customerMeal.meal),
    __metadata("design:type", Array)
], Meal.prototype, "reviews", void 0);
__decorate([
    typeorm_1.OneToMany(() => order_meal_1.default, (orderMeal) => orderMeal.meal),
    __metadata("design:type", Array)
], Meal.prototype, "mealOrders", void 0);
__decorate([
    typeorm_1.ManyToMany(() => offer_1.default, (offer) => offer.meals),
    __metadata("design:type", Array)
], Meal.prototype, "offers", void 0);
Meal = __decorate([
    typeorm_1.Entity({ name: "meal" })
], Meal);
exports.default = Meal;
