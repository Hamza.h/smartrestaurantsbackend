"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerRestaurantSubsriber = void 0;
const typeorm_1 = require("typeorm");
const customer_restaurant_1 = __importDefault(require("../models/customer_restaurant"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
let CustomerRestaurantSubsriber = class CustomerRestaurantSubsriber {
    // Denotes that this subscriber only listens to Trip Entity
    listenTo() {
        return customer_restaurant_1.default;
    }
    // Called after entity insertion
    afterInsert(event) {
        return __awaiter(this, void 0, void 0, function* () {
            updateRestaurantRating(event.entity);
        });
    }
    afterUpdate(event) {
        return __awaiter(this, void 0, void 0, function* () {
            updateRestaurantRating(event.entity);
        });
    }
};
CustomerRestaurantSubsriber = __decorate([
    typeorm_1.EventSubscriber()
], CustomerRestaurantSubsriber);
exports.CustomerRestaurantSubsriber = CustomerRestaurantSubsriber;
const updateRestaurantRating = (model) => {
    console.log("Trigger Activated");
    const customerRestaurantRepository = typeorm_1.getRepository(customer_restaurant_1.default);
    const restaurantRepository = typeorm_1.getRepository(restaurant_1.default);
    customerRestaurantRepository
        .findOne(model.id, {
        relations: ["restaurant"],
    })
        .then((customerRestaurant) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(customerRestaurant);
        if (customerRestaurant && customerRestaurant.restaurant) {
            const { avg } = yield customerRestaurantRepository
                .createQueryBuilder()
                .select("AVG(restaurant_rate)", "avg")
                .where("restaurant_id = :id", {
                id: customerRestaurant.restaurant.id,
            })
                .getRawOne();
            console.log(avg);
            if (avg) {
                customerRestaurant.restaurant.rating = avg;
                restaurantRepository.update(customerRestaurant.restaurant.id, customerRestaurant.restaurant);
            }
        }
    }))
        .catch((error) => {
        console.log(error);
    });
};
