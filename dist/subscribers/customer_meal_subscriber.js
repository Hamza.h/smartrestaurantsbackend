"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerMealSubsriber = void 0;
const typeorm_1 = require("typeorm");
const customer_meal_1 = __importDefault(require("../models/customer_meal"));
const meal_1 = __importDefault(require("../models/meal"));
const updateMealRating = (model) => {
    console.log("Meal Trigger Activate");
    console.log(model);
    const customerMealRepository = typeorm_1.getRepository(customer_meal_1.default);
    const mealRepository = typeorm_1.getRepository(meal_1.default);
    customerMealRepository
        .findOne(model.id, {
        relations: ["meal"],
    })
        .then((customerMeal) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(customerMeal);
        if (customerMeal && customerMeal.meal) {
            const { avg } = yield customerMealRepository
                .createQueryBuilder()
                .select("AVG(meal_rate)", "avg")
                .where("meal_id = :id", { id: customerMeal.meal.id })
                .getRawOne();
            customerMeal.meal.rating = avg;
            mealRepository.update(customerMeal.meal.id, customerMeal.meal);
        }
    }))
        .catch((error) => {
        console.log(error);
    });
};
let CustomerMealSubsriber = class CustomerMealSubsriber {
    // Denotes that this subscriber only listens to Trip Entity
    listenTo() {
        return customer_meal_1.default;
    }
    // Called after entity insertion
    afterInsert(event) {
        return __awaiter(this, void 0, void 0, function* () {
            updateMealRating(event.entity);
        });
    }
    afterUpdate(event) {
        return __awaiter(this, void 0, void 0, function* () {
            updateMealRating(event.entity);
        });
    }
};
CustomerMealSubsriber = __decorate([
    typeorm_1.EventSubscriber()
], CustomerMealSubsriber);
exports.CustomerMealSubsriber = CustomerMealSubsriber;
