"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authValidator = exports.signUpCustomerValidator = exports.signUpRestaurantValidator = void 0;
const express_validator_1 = require("express-validator");
const restaurant_1 = __importDefault(require("../models/restaurant"));
const typeorm_1 = require("typeorm");
const customer_1 = __importDefault(require("../models/customer"));
exports.signUpRestaurantValidator = [
    express_validator_1.body("restaurant").exists().withMessage("Please send restaurant object"),
    express_validator_1.body("restaurant.restaurantName")
        .exists()
        .withMessage("Please send restaurantName parameter"),
    express_validator_1.body("restaurant.address")
        .exists()
        .withMessage("Please send address parameter"),
    express_validator_1.body("restaurant.latitude")
        .exists()
        .withMessage("Please send latitude parameter"),
    express_validator_1.body("restaurant.longitude")
        .exists()
        .withMessage("Please send longitude parameter"),
    express_validator_1.body("restaurant.username")
        .exists()
        .withMessage("Please send username parameter"),
    express_validator_1.body("restaurant.password")
        .exists()
        .withMessage("Please send password parameter"),
    express_validator_1.body("contact").exists().withMessage("Please send contact object"),
    express_validator_1.body("contact.label").exists().withMessage("Please send label parameter"),
    express_validator_1.body("contact.phoneNumber")
        .exists()
        .withMessage("Please send phoneNumber parameter"),
    express_validator_1.body("contact.*").notEmpty().withMessage("missing value in contact"),
    express_validator_1.body("foodCategories").exists().withMessage("foodCategories is empty"),
    express_validator_1.body("restaurant.username").custom((value) => {
        const restaurantRepository = typeorm_1.getRepository(restaurant_1.default);
        const customerRepository = typeorm_1.getRepository(customer_1.default);
        return restaurantRepository
            .findOne({
            where: {
                username: value,
            },
        })
            .then((result) => {
            if (result)
                throw new Error("username already exists");
            return customerRepository
                .findOne({
                where: {
                    username: value,
                },
            })
                .then((result) => {
                if (result)
                    throw new Error("username already exists");
            });
        });
    }),
];
exports.signUpCustomerValidator = [
    express_validator_1.body("firstName").exists().withMessage("Please send firstName parameter"),
    express_validator_1.body("lastName").exists().withMessage("Please send lastName parameter"),
    express_validator_1.body("username").exists().withMessage("Please send username parameter"),
    express_validator_1.body("password").exists().withMessage("Please send password parameter"),
    express_validator_1.body("phoneNumber").exists().withMessage("Please send phoneNumber parameter"),
    express_validator_1.body("foodCategories")
        .exists()
        .notEmpty()
        .withMessage("foodCategories is empty"),
    express_validator_1.body("username").custom((value) => {
        const restaurantRepository = typeorm_1.getRepository(restaurant_1.default);
        const customerRepository = typeorm_1.getRepository(customer_1.default);
        return restaurantRepository
            .findOne({
            where: {
                username: value,
            },
        })
            .then((result) => {
            if (result)
                throw new Error("username already exists");
            return customerRepository
                .findOne({
                where: {
                    username: value,
                },
            })
                .then((result) => {
                if (result)
                    throw new Error("username already exists");
            });
        });
    }),
];
exports.authValidator = [
    express_validator_1.body("username").exists().withMessage("Please send username parameter"),
    express_validator_1.body("password").exists().withMessage("Please send password parameter"),
    express_validator_1.body("*").notEmpty().withMessage("missing arguments"),
];
