import { Router } from "express";
import { isAuth } from "../middlewares/auth_middleware";
import {
  searchRestaurant,
  reviewRestaurant,
  getRestaurantByID,
  getAllReviews,
  getAllRestaurants,
  favouriteRestaurant,
  getMostPopularRestaurants,
  getRestaurantMostOrderedMeals,
  getRestaurantMostOrderedOffers,
} from "../controllers/restaurant_controller";

const router = Router();

router.use(isAuth);

router.get("/mostOrderedMeals/:restaurantId?", getRestaurantMostOrderedMeals);

router.get("/mostOrderedOffers/:restaurantId?", getRestaurantMostOrderedOffers);

router.get("/mostPopular", getMostPopularRestaurants);

router.get("/search/:name?", searchRestaurant);

router.get("/byId/:restaurantId?", getRestaurantByID);

router.get("/reviews/:restaurantId?:page?", getAllReviews);

router.get("/", getAllRestaurants);

router.post("/review", reviewRestaurant);

router.post("/favourite/:restaurantId?", favouriteRestaurant);

export default router;
