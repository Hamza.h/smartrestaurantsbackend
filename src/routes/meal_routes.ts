import { Router } from "express";
import { isAuth } from "../middlewares/auth_middleware";
import {
  addMeal,
  editMeal,
  deleteMeal,
  favouriteMeal,
  reviewMeal,
  getMealByID,
  searchMeal,
  getAllMeals,
  getAllReviews,
  getMostOrderedMeals,
} from "../controllers/meal_controller";

const router = Router();

router.use(isAuth);

router.get("/mostOrderedMeals", getMostOrderedMeals);

router.get("/search/:mealName?", searchMeal);

router.get("/review/:page?:mealId?", getAllReviews);

router.get("/byId/:mealId?", getMealByID);

router.get("/:page?", getAllMeals);

router.post("/favourite", favouriteMeal);

router.post("/review", reviewMeal);

router.post("/", addMeal);

router.put("/", editMeal);

router.delete("/:mealId?", deleteMeal);

export default router;
