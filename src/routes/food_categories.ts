import { Router } from "express";
import { getFoodCategories } from "../controllers/food_categories_controller";

const router = Router();

router.get("/", getFoodCategories);

export default router;
