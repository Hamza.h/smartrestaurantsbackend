import { Router } from "express";
import { isAuth } from "../middlewares/auth_middleware";
import {
  getFavouiteMeals,
  editCustomerFoodCategories,
  getFavouriteRestaurants,
  changePassword,
  editCustomerProfile,
  editRestaurantContacts,
  editRestaurantProfile,
  editRestaurantFoodCategories,
} from "../controllers/profile_controller";

const router = Router();

router.use(isAuth);

router.get("/favouriteMeals", getFavouiteMeals);

router.get("/favouriteRestaurants", getFavouriteRestaurants);

router.put("/editCustomerFoodCategories", editCustomerFoodCategories);

router.put("/changePassword", changePassword);

router.put("/customer", editCustomerProfile);

router.put("/contacts", editRestaurantContacts);

router.put("/restaurant", editRestaurantProfile);

router.put("/editRestaurantFoodCategories", editRestaurantFoodCategories);

export default router;
