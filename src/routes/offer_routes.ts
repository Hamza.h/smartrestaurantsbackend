import { Router } from "express";
import { isAuth } from "../middlewares/auth_middleware";
import {
  createOffer,
  editOffer,
  deleteOffer,
  getOfferByID,
  searchOffer,
  getAllOffers,
  getMostOrderedOffers,
} from "../controllers/offer_controller";

const router = Router();

router.use(isAuth);

router.post("/", createOffer);

router.put("/", editOffer);

router.delete("/:offerId?", deleteOffer);

router.get("/mostOrderedOffers", getMostOrderedOffers);

router.get("/search/:offerName?", searchOffer);

router.get("/byId/:offerId?", getOfferByID);

router.get("/:page?", getAllOffers);

export default router;
