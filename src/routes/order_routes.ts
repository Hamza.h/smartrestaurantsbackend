import { Router } from "express";
import { isAuth } from "../middlewares/auth_middleware";
import {
  createOrder,
  getUnfinishedOrders,
  getFinishedOrders,
  getOrderByID,
  getPendingOrders,
  getProcessingOrders,
  getDeliveringOrders,
  changeOrderState,
} from "../controllers/order_controller";

const router = Router();

router.use(isAuth);

router.post("/state", changeOrderState);

router.post("/", createOrder);

router.get("/unFinished", getUnfinishedOrders);

router.get("/finished", getFinishedOrders);

router.get("/byId/:orderId?", getOrderByID);

router.get("/pending", getPendingOrders);

router.get("/processing", getProcessingOrders);

router.get("/delivering", getDeliveringOrders);

export default router;
