import { Router } from "express";
import authRoutes from "./auth";
import foodCategoriesRoutes from "./food_categories";
import { uploadController } from "../controllers/upload_controller";
import basicMiddlewares from "../middlewares/basics";
import mealRoutes from "./meal_routes";
import offerRoutes from "./offer_routes";
import orderRoutes from "./order_routes";
import restaurantsRoutes from "./restaurant_routes";
import profileRoutes from "./profile_routes";
import multer from "multer";

const fileUpload = multer();

const router = Router();

router.post("/uploadImage", fileUpload.single("image"), uploadController);

router.use(basicMiddlewares);

router.use("/auth", authRoutes);

router.use("/meals", mealRoutes);

router.use("/offers", offerRoutes);

router.use("/orders", orderRoutes);

router.use("/foodCategories", foodCategoriesRoutes);

router.use("/restaurants", restaurantsRoutes);

router.use("/profile", profileRoutes);

export default router;
