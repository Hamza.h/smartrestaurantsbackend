import { Router } from "express";
import {
  auth,
  signUpRestaurant,
  signUpCustomer,
} from "../controllers/auth_controller";
import {
  signUpRestaurantValidator,
  authValidator,
  signUpCustomerValidator,
} from "../validators/auth";

const router = Router();

router.post("/", authValidator, auth);

router.post("/signUpRestaurant", signUpRestaurantValidator, signUpRestaurant);

router.post("/signUpCustomer", signUpCustomerValidator, signUpCustomer);

export default router;
