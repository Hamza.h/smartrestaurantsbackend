import { body } from "express-validator";
import Restaurant from "../models/restaurant";
import { getRepository } from "typeorm";
import Customer from "../models/customer";

export const signUpRestaurantValidator = [
  body("restaurant").exists().withMessage("Please send restaurant object"),
  body("restaurant.restaurantName")
    .exists()
    .withMessage("Please send restaurantName parameter"),
  body("restaurant.address")
    .exists()
    .withMessage("Please send address parameter"),
  body("restaurant.latitude")
    .exists()
    .withMessage("Please send latitude parameter"),
  body("restaurant.longitude")
    .exists()
    .withMessage("Please send longitude parameter"),
  body("restaurant.username")
    .exists()
    .withMessage("Please send username parameter"),
  body("restaurant.password")
    .exists()
    .withMessage("Please send password parameter"),
  body("contact").exists().withMessage("Please send contact object"),
  body("contact.label").exists().withMessage("Please send label parameter"),
  body("contact.phoneNumber")
    .exists()
    .withMessage("Please send phoneNumber parameter"),
  body("contact.*").notEmpty().withMessage("missing value in contact"),
  body("foodCategories").exists().withMessage("foodCategories is empty"),
  body("restaurant.username").custom((value) => {
    const restaurantRepository = getRepository(Restaurant);
    const customerRepository = getRepository(Customer);
    return restaurantRepository
      .findOne({
        where: {
          username: value,
        },
      })
      .then((result) => {
        if (result) throw new Error("username already exists");
        return customerRepository
          .findOne({
            where: {
              username: value,
            },
          })
          .then((result) => {
            if (result) throw new Error("username already exists");
          });
      });
  }),
];

export const signUpCustomerValidator = [
  body("firstName").exists().withMessage("Please send firstName parameter"),
  body("lastName").exists().withMessage("Please send lastName parameter"),
  body("username").exists().withMessage("Please send username parameter"),
  body("password").exists().withMessage("Please send password parameter"),
  body("phoneNumber").exists().withMessage("Please send phoneNumber parameter"),
  body("foodCategories")
    .exists()
    .notEmpty()
    .withMessage("foodCategories is empty"),
  body("username").custom((value) => {
    const restaurantRepository = getRepository(Restaurant);
    const customerRepository = getRepository(Customer);
    return restaurantRepository
      .findOne({
        where: {
          username: value,
        },
      })
      .then((result) => {
        if (result) throw new Error("username already exists");
        return customerRepository
          .findOne({
            where: {
              username: value,
            },
          })
          .then((result) => {
            if (result) throw new Error("username already exists");
          });
      });
  }),
];

export const authValidator = [
  body("username").exists().withMessage("Please send username parameter"),
  body("password").exists().withMessage("Please send password parameter"),
  body("*").notEmpty().withMessage("missing arguments"),
];
