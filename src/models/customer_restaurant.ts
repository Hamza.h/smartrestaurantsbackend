import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import Customer from "./customer";
import Restaurant from "./restaurant";

@Entity({ name: "customer_restaurant" })
export default class CustomerRestaurant {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "bool", default: false, name: "is_favourite" })
  isFavourite: boolean;

  @Column({ nullable: true })
  comment: string;

  @Column({ type: "int", name: "restaurant_rate", nullable: true })
  restaurantRate: number;

  @ManyToOne(() => Customer, (customer) => customer.customerRestaurants)
  @JoinColumn({ name: "customer_id", referencedColumnName: "id" })
  customer: Customer;

  @ManyToOne(() => Restaurant, (restaunrant) => restaunrant.reviews)
  @JoinColumn({ name: "restaurant_id", referencedColumnName: "id" })
  restaurant: Restaurant;
}
