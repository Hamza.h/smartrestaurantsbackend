import {Entity , PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import RestaurantFoodCategory from "./restaurant_food_category"

@Entity({name: "food_category"})
export default class FoodCategory {

  @PrimaryGeneratedColumn()
  id: number

  @Column({name: "category_name"})
  categoryName:string

  @Column({name: "image_url"})
  imageUrl:string

  @Column({name: "category_color"})
  categoryColor:string

  @OneToMany(() => RestaurantFoodCategory, restaurantFoodCategory => restaurantFoodCategory.foodCategory)
  restaurantFoodCategories: RestaurantFoodCategory[]
  
}
