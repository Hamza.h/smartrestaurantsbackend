import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
} from "typeorm";
import FoodCategory from "./food_category";
import CustomerRestaurant from "./customer_restaurant";
import Order from "./order";
import CustomerMeal from "./customer_meal";
import { defaultCustomerImageUrl } from "../utils/constants";

@Entity({ name: "customer" })
export default class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "first_name" })
  firstName: string;

  @Column({ name: "last_name" })
  lastName: string;

  @Column({ name: "phone_number" })
  phoneNumber: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({ name: "image_url", default: defaultCustomerImageUrl })
  imageUrl: string;

  @ManyToMany(() => FoodCategory, {
    cascade: true,
  })
  @JoinTable({
    name: "customer_food_category",
    joinColumn: {
      name: "customer_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "food_category_id",
      referencedColumnName: "id",
    },
  })
  foodCategories: FoodCategory[];

  @OneToMany(
    () => CustomerRestaurant,
    (customerRestaurant) => customerRestaurant.customer
  )
  customerRestaurants: CustomerRestaurant[];

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];

  @OneToMany(() => CustomerMeal, (customerMeal) => customerMeal.customer)
  customerMeals: CustomerMeal[];
}
