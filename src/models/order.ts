import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from "typeorm";
import Restaurant from "./restaurant";
import Customer from "./customer";
import OrderStatus from "./order_status";
import OrderMeal from "./order_meal";

@Entity({ name: "order" })
export default class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: "order_date",
    default: () => "CURRENT_TIMESTAMP",
    type: "timestamp",
  })
  orderDate: Date;

  @Column({ name: "order_tax", default: 0.0 })
  orderTax: number;

  @Column({ name: "customer_lat", nullable: true })
  customerLat: number;

  @Column({ name: "customer_long", nullable: true })
  customerLong: number;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.orders)
  @JoinColumn({ name: "restaurant_id", referencedColumnName: "id" })
  restaurant: Restaurant;

  @ManyToOne(() => Customer, (customer) => customer.orders)
  @JoinColumn({ name: "customer_id", referencedColumnName: "id" })
  customer: Customer;

  @ManyToOne(() => OrderStatus, (orderStatus) => orderStatus.orders)
  @JoinColumn({ name: "status_id", referencedColumnName: "id" })
  status: OrderStatus;

  @OneToMany(() => OrderMeal, (orderMeal) => orderMeal.order, {
    cascade: true,
  })
  orderMeals: OrderMeal[];
}
