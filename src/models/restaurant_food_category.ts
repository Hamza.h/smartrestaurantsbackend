import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from "typeorm";
import Restaurant from "./restaurant";
import FoodCategory from "./food_category";
import Meal from "./meal";

@Entity({ name: "restaurant_food_category" })
export default class RestaurantFoodCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Restaurant,
    (restaurant) => restaurant.restaurantFoodCategories
  )
  @JoinColumn({ name: "restaurant_id", referencedColumnName: "id" })
  restaurant: Restaurant;

  @ManyToOne(
    () => FoodCategory,
    (foodCategory) => foodCategory.restaurantFoodCategories
  )
  @JoinColumn({ name: "food_category_id", referencedColumnName: "id" })
  foodCategory: FoodCategory;

  @OneToMany(() => Meal, (meal) => meal.restaurantFoodCategory)
  meals: Meal[];
}
