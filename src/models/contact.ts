import {Entity , PrimaryGeneratedColumn, Column, ManyToOne , JoinColumn} from "typeorm";
import Restaurant from "./restaurant"


@Entity({name: "contact"})
export default class Contact {

  @PrimaryGeneratedColumn()
  id: number

  @Column({nullable: false})
  label: string

  @Column({nullable: false, name: "phone_number"})
  phoneNumber: string

  @ManyToOne(() => Restaurant , restaurant => restaurant.contacts)
  @JoinColumn({name: "restaurant_id", referencedColumnName: "id"})
  restaurant: Restaurant

}