import {Entity , PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import Order from "./order"


@Entity({name: "order_status"})
export default class OrderStatus {

  @PrimaryGeneratedColumn()
  id: number

  @Column({unique: true})
  label: string

  @Column({unique: true})
  color: string

  @OneToMany(() => Order , order => order.status)
  orders: Order[]
}