import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinTable,
  OneToMany,
} from "typeorm";
import RestaurantFoodCategory from "./restaurant_food_category";
import CustomerRestaurant from "./customer_restaurant";
import Contact from "./contact";
import Offer from "./offer";
import Order from "./order";
import { defaultRestaurantImageUrl } from "../utils/constants";

@Entity({ name: "restaurant" })
export default class Restaurant {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "restaurant_name" })
  restaurantName: string;

  @Column({ name: "lat", type: "double" })
  latitude: number;

  @Column({ name: "long", type: "double" })
  longitude: number;

  @Column({ name: "address" })
  address: string;

  @Column({ name: "has_delivery", default: false })
  hasDelivery: boolean;

  @Column({ name: "has_tables", default: false })
  hasTables: boolean;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({
    name: "image_url",
    nullable: true,
    default: defaultRestaurantImageUrl,
  })
  imageUrl: string;

  @Column({ nullable: true })
  rating: number;

  @Column({ name: "order_tax", default: 0.0 })
  orderTax: number;

  @OneToMany(
    () => RestaurantFoodCategory,
    (restaurantFoodCategory) => restaurantFoodCategory.restaurant
  )
  restaurantFoodCategories: RestaurantFoodCategory[];

  @OneToMany(
    () => CustomerRestaurant,
    (customerRestaurant) => customerRestaurant.restaurant
  )
  reviews: CustomerRestaurant[];

  @OneToMany(() => Contact, (contact) => contact.restaurant, { cascade: true })
  contacts: Contact[];

  @OneToMany(() => Offer, (offer) => offer.restaurant)
  offers: Offer[];

  @OneToMany(() => Order, (order) => order.restaurant)
  orders: Order[];
}
