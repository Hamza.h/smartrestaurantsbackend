import {Entity , PrimaryGeneratedColumn, Column, ManyToOne , JoinColumn} from "typeorm";
import Order from "./order"
import Meal from "./meal";
import Offer from "./offer"

@Entity({name: "order_meal"})
export default class OrderMeal {

  @PrimaryGeneratedColumn()
  id: number

  @Column({name: "item_price"})
  itemPrice: number

  @Column({name: "item_quantity", type: "int"})
  itemQuantity: number

  @ManyToOne(() => Order , order => order.orderMeals)
  @JoinColumn({name: "order_id", referencedColumnName: "id"})
  order: Order

  @ManyToOne(() => Meal, meal => meal.mealOrders)
  @JoinColumn({name: "meal_id", referencedColumnName: "id"})
  meal: Meal

  @ManyToOne(() => Offer, offer => offer.offerOrders)
  @JoinColumn({name: "offer_id", referencedColumnName: "id"})
  offer: Offer
}