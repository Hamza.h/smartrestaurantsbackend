import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
  DeleteDateColumn,
} from "typeorm";
import Restaurant from "./restaurant";
import OrderMeal from "./order_meal";
import Meal from "./meal";
import { defaultMealOfferImageUrl } from "../utils/constants";

@Entity({ name: "offer" })
export default class Offer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "offer_name" })
  offerName: string;

  @Column({
    name: "start_date",
    default: () => "CURRENT_TIMESTAMP",
    type: "timestamp",
  })
  startDate: Date;

  @Column({ name: "end_date" })
  endDate: Date;

  @Column({ nullable: true })
  description: string;

  @Column()
  price: number;

  @Column({
    name: "image_url",
    nullable: true,
    default: defaultMealOfferImageUrl,
  })
  imageUrl: string;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(() => Restaurant, (restaurant) => restaurant.offers)
  @JoinColumn({ name: "restaurant_id", referencedColumnName: "id" })
  restaurant: Restaurant;

  @OneToMany(() => OrderMeal, (orderMeal) => orderMeal.offer)
  offerOrders: OrderMeal[];

  @ManyToMany(() => Meal, (meal) => meal.offers)
  @JoinTable({
    name: "offer_meal",
    joinColumn: {
      name: "offer_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "meal_id",
      referencedColumnName: "id",
    },
  })
  meals: Meal[];
}
