import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
  ManyToMany,
  DeleteDateColumn,
} from "typeorm";
import RestaurantFoodCategory from "./restaurant_food_category";
import CustomerMeal from "./customer_meal";
import OrderMeal from "./order_meal";
import Offer from "./offer";
import { defaultMealOfferImageUrl } from "../utils/constants";

@Entity({ name: "meal" })
export default class Meal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "meal_name" })
  mealName: string;

  @Column({ name: "meal_price" })
  mealPrice: number;

  @Column({ nullable: true })
  description: string;

  @Column({
    nullable: true,
    name: "image_url",
    default: defaultMealOfferImageUrl,
  })
  imageUrl: string;

  @Column({ name: "discount_percent", default: 0.0 })
  discountPercent: number;

  @Column({ nullable: true })
  rating: number;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(
    () => RestaurantFoodCategory,
    (restaurantFoodCategory) => restaurantFoodCategory.meals
  )
  @JoinColumn({
    name: "restaurant_food_category_id",
    referencedColumnName: "id",
  })
  restaurantFoodCategory: RestaurantFoodCategory;

  @OneToMany(() => CustomerMeal, (customerMeal) => customerMeal.meal)
  reviews: CustomerMeal[];

  @OneToMany(() => OrderMeal, (orderMeal) => orderMeal.meal)
  mealOrders: OrderMeal[];

  @ManyToMany(() => Offer, (offer) => offer.meals)
  offers: Offer[];
}
