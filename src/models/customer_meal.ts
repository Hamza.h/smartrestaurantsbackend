import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import Customer from "./customer";
import Meal from "./meal";

@Entity({ name: "customer_meal" })
export default class CustomerMeal {
  @PrimaryGeneratedColumn({ type: "int" })
  id: number;

  @Column({ type: "int", name: "meal_rate", nullable: true })
  mealRate: number;

  @Column({ type: "bool", name: "is_favourite", default: false })
  isFavourite: boolean;

  @Column({ nullable: true })
  comment: string;

  @ManyToOne(() => Customer, (customer) => customer.customerMeals)
  @JoinColumn({ name: "customer_id", referencedColumnName: "id" })
  customer: Customer;

  @ManyToOne(() => Meal, (meal) => meal.reviews)
  @JoinColumn({ name: "meal_id", referencedColumnName: "id" })
  meal: Meal;
}
