import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
  getRepository,
} from "typeorm";
import CustomerRestaurant from "../models/customer_restaurant";
import Restaurant from "../models/restaurant";

@EventSubscriber()
export class CustomerRestaurantSubsriber
  implements EntitySubscriberInterface<CustomerRestaurant>
{
  // Denotes that this subscriber only listens to Trip Entity
  listenTo() {
    return CustomerRestaurant;
  }

  // Called after entity insertion
  async afterInsert(event: InsertEvent<any>) {
    updateRestaurantRating(event.entity);
  }

  async afterUpdate(event: UpdateEvent<any>) {
    updateRestaurantRating(event.entity);
  }
}

const updateRestaurantRating = (model: CustomerRestaurant) => {
  console.log("Trigger Activated");
  const customerRestaurantRepository = getRepository(CustomerRestaurant);
  const restaurantRepository = getRepository(Restaurant);
  customerRestaurantRepository
    .findOne(model.id, {
      relations: ["restaurant"],
    })
    .then(async (customerRestaurant) => {
      console.log(customerRestaurant);
      if (customerRestaurant && customerRestaurant.restaurant) {
        const { avg } = await customerRestaurantRepository
          .createQueryBuilder()
          .select("AVG(restaurant_rate)", "avg")
          .where("restaurant_id = :id", {
            id: customerRestaurant.restaurant.id,
          })
          .getRawOne();
        console.log(avg);
        if (avg) {
          customerRestaurant.restaurant.rating = avg;
          restaurantRepository.update(
            customerRestaurant.restaurant.id,
            customerRestaurant.restaurant
          );
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });
};
