import {
  getRepository,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  EntitySubscriberInterface,
} from "typeorm";
import CustomerMeal from "../models/customer_meal";
import Meal from "../models/meal";

const updateMealRating = (model: CustomerMeal) => {
  console.log("Meal Trigger Activate");
  console.log(model);
  const customerMealRepository = getRepository(CustomerMeal);
  const mealRepository = getRepository(Meal);
  customerMealRepository
    .findOne(model.id, {
      relations: ["meal"],
    })
    .then(async (customerMeal) => {
      console.log(customerMeal);
      if (customerMeal && customerMeal.meal) {
        const { avg } = await customerMealRepository
          .createQueryBuilder()
          .select("AVG(meal_rate)", "avg")
          .where("meal_id = :id", { id: customerMeal.meal.id })
          .getRawOne();
        customerMeal.meal.rating = avg;
        mealRepository.update(customerMeal.meal.id, customerMeal.meal);
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

@EventSubscriber()
export class CustomerMealSubsriber
  implements EntitySubscriberInterface<CustomerMeal>
{
  // Denotes that this subscriber only listens to Trip Entity
  listenTo() {
    return CustomerMeal;
  }

  // Called after entity insertion
  async afterInsert(event: InsertEvent<CustomerMeal>) {
    updateMealRating(event.entity);
  }

  async afterUpdate(event: UpdateEvent<CustomerMeal>) {
    updateMealRating(event.entity);
  }
}
