export const encryptSecretKey = "app-restaurant-smart";
export const defaultCustomerImageUrl =
  "https://res.cloudinary.com/smart-restaurants-storage/image/upload/v1625055408/images/user_tf9yzu.png";
export const defaultRestaurantImageUrl =
  "https://res.cloudinary.com/smart-restaurants-storage/image/upload/v1625066627/images/depositphotos_24009929_stock_illustration_knife_fork_and_spoon_rdgzvm.jpg";
export const defaultMealOfferImageUrl =
  "https://res.cloudinary.com/smart-restaurants-storage/image/upload/v1625055410/images/meal_vkjgxf.png";
export const PENDING_STATE = 1;
export const PROCESSING_STATE = 2;
export const DELIVERING_STATE = 3;
export const FINISHED_STATE = 4;
export const REJECTED_STATE = 6;
