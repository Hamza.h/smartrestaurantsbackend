import { Request, Response, NextFunction } from "express";
import { getRepository, Not, IsNull } from "typeorm";
import Meal from "../models/meal";
import CustomerMeal from "../models/customer_meal";
import RestaurantFoodCategory from "../models/restaurant_food_category";
import OrderMeal from "../models/order_meal";
import { defaultMealOfferImageUrl } from "../utils/constants";

export const addMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealName = req.body.mealName;
    const mealPrice = req.body.mealPrice;
    const description = req.body.description;
    let imageUrl = req.body.imageUrl;
    const discountPercent = req.body.discountPercent;
    const foodCategoryId = req.body.foodCategoryId;

    if (!mealName) return res.status(400).json({ message: "Missing mealName" });
    if (!mealPrice)
      return res.status(400).json({ message: "Missing mealPrice" });
    if (!description)
      return res.status(400).json({ message: "Missing description" });
    if (!imageUrl) imageUrl = defaultMealOfferImageUrl;
    if (discountPercent == null)
      return res.status(400).json({ message: "Missing discountPercent" });
    if (!foodCategoryId)
      return res.status(400).json({ message: "Missing foodCategoryId" });

    const restaurantFoodCategoryId = (
      await getRepository(RestaurantFoodCategory).findOne({
        where: {
          foodCategory: { id: foodCategoryId },
          restaurant: { id: req.body.restaurant.id },
        },
      })
    )?.id;
    getRepository(Meal)
      .save({
        mealName: mealName,
        mealPrice: mealPrice,
        description: description,
        imageUrl: imageUrl,
        discountPercent: discountPercent,
        restaurantFoodCategory: { id: restaurantFoodCategoryId },
      })
      .then((result) => {
        return res.status(200).json({ message: "Success" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const editMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId = req.body.mealId;
    const mealName = req.body.mealName;
    const mealPrice = req.body.mealPrice;
    const description = req.body.description;
    let imageUrl = req.body.imageUrl;
    const discountPercent = req.body.discountPercent;
    const foodCategoryId = req.body.foodCategoryId;

    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    if (!mealName) return res.status(400).json({ message: "Missing mealName" });
    if (!mealPrice)
      return res.status(400).json({ message: "Missing mealPrice" });
    if (!description)
      return res.status(400).json({ message: "Missing description" });
    if (!imageUrl) imageUrl = defaultMealOfferImageUrl;
    if (discountPercent == null)
      return res.status(400).json({ message: "Missing discountPercent" });
    if (!foodCategoryId)
      return res.status(400).json({ message: "Missing foodCategoryId" });

    const restaurantFoodCategoryId = (
      await getRepository(RestaurantFoodCategory).findOne({
        where: {
          foodCategory: { id: foodCategoryId },
          restaurant: { id: req.body.restaurant.id },
        },
      })
    )?.id;
    getRepository(Meal)
      .update(mealId, {
        mealName: mealName,
        mealPrice: mealPrice,
        description: description,
        imageUrl: imageUrl,
        discountPercent: discountPercent,
        restaurantFoodCategory: { id: restaurantFoodCategoryId },
      })
      .then((result) => {
        return res.status(200).json({ message: "Success" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId: any = req.query.mealId;
    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    const mealRepository = getRepository(Meal);
    mealRepository
      .findOne(mealId, {
        relations: [
          "restaurantFoodCategory",
          "restaurantFoodCategory.restaurant",
        ],
      })
      .then((meal) => {
        if (meal) {
          if (
            meal.restaurantFoodCategory.restaurant.id == req.body.restaurant.id
          ) {
            mealRepository
              .softDelete(meal)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                return res.status(500).json({ message: error.message });
              });
          } else return res.status(403).json({ message: "Forbidden" });
        } else return res.status(404).json({ message: "Meal Not Found" });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const favouriteMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId = req.body.mealId;
    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    const customerMealRepository = getRepository(CustomerMeal);
    customerMealRepository
      .findOne({
        where: {
          customer: { id: req.body.customer.id },
          meal: { id: mealId },
        },
      })
      .then((customerMeal) => {
        if (customerMeal) {
          customerMeal.isFavourite = !customerMeal.isFavourite;
          customerMealRepository
            .save(customerMeal)
            .then((result) => {
              return res.status(200).json({ message: "Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else {
          customerMealRepository
            .save({
              customer: { id: req.body.customer.id },
              meal: { id: mealId },
              isFavourite: true,
            })
            .then((result) => {
              return res.status(200).json({ message: "Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        }
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const reviewMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId = req.body.mealId;
    const mealRate = req.body.mealRate;
    const comment = req.body.comment;
    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    if (!mealRate) return res.status(400).json({ message: "Missing mealRate" });
    if (!comment) return res.status(400).json({ message: "Missing comment" });
    const customerMealRepository = getRepository(CustomerMeal);
    customerMealRepository
      .findOne({
        where: {
          customer: { id: req.body.customer.id },
          meal: { id: mealId },
        },
      })
      .then((customerMeal) => {
        if (customerMeal && customerMeal.mealRate) {
          return res
            .status(400)
            .json({ message: "You have been already review this meal" });
        } else {
          if (!customerMeal) {
            customerMealRepository
              .save({
                customer: { id: req.body.customer.id },
                meal: { id: mealId },
                mealRate: mealRate,
                comment: comment,
              })
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                console.log(error);
                return res.status(500).json({ message: error.message });
              });
          } else {
            customerMeal.mealRate = mealRate;
            customerMeal.comment = comment;
            customerMealRepository
              .save(customerMeal)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                console.log(error);
                return res.status(500).json({ message: error.message });
              });
          }
        }
      })
      .catch((error) => {
        console.log(error);
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: error.message });
  }
};

export const getMealByID = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId: any = req.query.mealId;
    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    const mealRepository = getRepository(Meal);
    mealRepository
      .findOne(mealId, {
        relations: [
          "restaurantFoodCategory",
          "restaurantFoodCategory.foodCategory",
          "restaurantFoodCategory.restaurant",
          "reviews",
          "reviews.customer",
        ],
      })
      .then((meal) => {
        if (meal) {
          meal.reviews = meal.reviews.slice(0, 2);
          let deleteReviews: any = [];
          const isCustomer = req.body.customer;
          let customerID: any;
          if (isCustomer) customerID = req.body.customer.id;
          meal.reviews.forEach((customerMeal: any) => {
            if (isCustomer && customerID == customerMeal.customer.id)
              Object.assign(meal, { isFavourite: customerMeal.isFavourite });
            if (customerMeal.mealRate != null && customerMeal.comment != null) {
              delete customerMeal.customer.password;
              delete customerMeal.customer.phoneNumber;
              delete customerMeal.customer.username;
            } else deleteReviews.push(customerMeal);
          });
          meal.reviews = meal.reviews.filter((e) => {
            if (!deleteReviews.find((element: any) => e.id == element.id))
              return e;
          });
          return res.status(200).json(meal);
        } else return res.status(404).json({ message: "Meal Not Found" });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const searchMeal = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealName = req.query.mealName;
    if (!mealName) return res.status(400).json({ message: "Missing mealName" });
    getRepository(Meal)
      .createQueryBuilder()
      .select()
      .where("meal_name like :label", { label: `%${mealName}%` })
      .getMany()
      .then((meals) => {
        return res.status(200).json(meals);
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getAllMeals = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const page: any = req.query.page ?? 1;
    const pageCount = 10;
    let foodCategoriesIDs;
    if (req.body.restaurant) {
      foodCategoriesIDs = (
        await getRepository(RestaurantFoodCategory).find({
          where: {
            restaurant: { id: req.body.restaurant.id },
          },
        })
      ).map((e) => e.id);
    } else {
      const customerFoodCategories = req.body.customer.foodCategories.map(
        (e: any) => e.id
      );
      foodCategoriesIDs = (
        await getRepository(RestaurantFoodCategory)
          .createQueryBuilder()
          .select("id")
          .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
          .getRawMany()
      ).map((e) => e.id);
    }
    getRepository(Meal)
      .createQueryBuilder()
      .select()
      .where("restaurant_food_category_id IN (:ids)", {
        ids: foodCategoriesIDs,
      })
      .skip((page - 1) * pageCount)
      .take(pageCount)
      .getMany()
      .then((meals) => {
        return res.status(200).json(meals);
      })
      .catch((err) => res.status(500).json({ message: err.message }));
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getAllReviews = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const mealId = req.query.mealId;
    const page: any = req.query.page ?? 1;
    const pageCount = 15;
    if (!mealId) return res.status(400).json({ message: "Missing mealId" });
    getRepository(CustomerMeal)
      .find({
        relations: ["customer"],
        where: {
          meal: { id: mealId },
          mealRate: Not(IsNull()),
          comment: Not(IsNull()),
        },
        skip: (page - 1) * pageCount,
        take: pageCount,
      })
      .then((reviews) => {
        return res.status(200).json(reviews);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getMostOrderedMeals = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantFoodCategoryRepository = getRepository(
      RestaurantFoodCategory
    );
    const mealRepository = getRepository(Meal);
    const orderMealRepository = getRepository(OrderMeal);
    let whereCond;
    let whereValue;
    if (req.body.customer) {
      whereCond = "food_category_id IN (:ids)";
      whereValue = {
        ids: req.body.customer.foodCategories.map((e: any) => e.id),
      };
    } else {
      whereCond = "restaurant_id IN (:id)";
      whereValue = {
        id: req.body.restaurant.id,
      };
    }
    restaurantFoodCategoryRepository
      .createQueryBuilder()
      .where(whereCond, whereValue)
      .distinct(true)
      .getMany()
      .then((restaurantFoodCategories) => {
        if (restaurantFoodCategories.length > 0) {
          mealRepository
            .createQueryBuilder()
            .where("restaurant_food_category_id IN (:selectedIDs)", {
              selectedIDs: restaurantFoodCategories.map((e) => e.id),
            })
            .getMany()
            .then((allMeals) => {
              if (allMeals.length > 0) {
                orderMealRepository
                  .createQueryBuilder()
                  .select("meal_id as mealId")
                  .addSelect("SUM(item_quantity) AS itemQuantity")
                  .where("meal_id IN (:meals)", {
                    meals: allMeals.map((e) => e.id),
                  })
                  .orderBy("itemQuantity", "DESC")
                  .groupBy("meal_id")
                  .getRawMany()
                  .then((items) => {
                    mealRepository
                      .findByIds(
                        items.map((e) => e.mealId),
                        {
                          take: 3,
                        }
                      )
                      .then((meals) => {
                        return res.status(200).json(meals);
                      })
                      .catch((err) => {
                        return res.status(500).json({ message: err.message });
                      });
                  })
                  .catch((error) => {
                    return res.status(500).json({ message: error.message });
                  });
              } else return res.status(404).json({ message: "No Meals Found" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "No Meals Found" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
    mealRepository.find;
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
