import { NextFunction, Request, Response } from "express";
import FoodCategory from "../models/food_category";
import Restaurant from "../models/restaurant";
import Customer from "../models/customer";
import Contact from "../models/contact";
import bcrypt from "bcrypt";
import { validationResult } from "express-validator";
import RestaurantFoodCategory from "../models/restaurant_food_category";
import jwt from "jsonwebtoken";
import { RestaurantRepository } from "../repositories/restaurant_repository";
import { encryptSecretKey } from "../utils/constants";
import { getRepository, getCustomRepository } from "typeorm";

export const auth = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        message: "Failed",
        error: errors.array()[0].msg,
      });
    }
    const username = req.body.username;
    const password = req.body.password;
    let loadedUser: Customer | Restaurant | any;
    let isCustomer: boolean = true;
    const customerRepository = getRepository(Customer);
    loadedUser = await customerRepository.findOne({
      where: { username: username },
      relations: ["foodCategories"],
    });
    if (!loadedUser) {
      isCustomer = false;
      const restaurantRepository = getCustomRepository(RestaurantRepository);
      loadedUser = await restaurantRepository.findOne({
        where: { username: username },
        relations: ["contacts"],
      });
      await restaurantRepository
        .getAllFoodCategories(loadedUser)
        .then((foodCategories) => {
          Object.assign(loadedUser, { foodCategories: foodCategories });
        })
        .catch((error) => {
          res.status(500).json({ message: "Failed", error: error.message });
        });
    }
    if (!loadedUser) {
      return res.status(404).json({ message: "Something Went Wrong" });
    }
    console.log(loadedUser);
    bcrypt
      .compare(password, loadedUser.password)
      .then((isEqual) => {
        if (!isEqual) {
          return res.status(422).json({ message: "Failed" });
        }
        const token = jwt.sign(
          {
            username: loadedUser?.username,
            isCustomer: isCustomer,
            date: Date.now(),
          },
          encryptSecretKey,
          { expiresIn: "7d" }
        );
        Object.assign(loadedUser, { jwtToken: token });
        delete loadedUser["password"];
        res.status(200).json({
          user: loadedUser,
          isCustomer: isCustomer,
        });
      })
      .catch((err) =>
        res.status(500).json({ message: "Failed", error: err.message })
      );
  } catch (err) {
    res.status(500).json({ message: "Failed", error: err.message });
  }
};

export const signUpRestaurant = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "Failed",
      errors: errors.array().map((err) => err),
    });
  }
  const restaurantRepository = getCustomRepository(RestaurantRepository);

  const restaurantFoodCategoryRepository = getRepository(
    RestaurantFoodCategory
  );

  const foodCategoryRepository = getRepository(FoodCategory);
  bcrypt.hash(req.body.restaurant.password, 12, (err, hasedPassword) => {
    const restaurant = new Restaurant();
    restaurant.hasDelivery = req.body.restaurant.hasDelivery;
    restaurant.hasTables = req.body.restaurant.hasTables;
    restaurant.restaurantName = req.body.restaurant.restaurantName;
    restaurant.address = req.body.restaurant.address;
    restaurant.latitude = req.body.restaurant.latitude;
    restaurant.longitude = req.body.restaurant.longitude;
    restaurant.username = req.body.restaurant.username;
    restaurant.password = hasedPassword;
    const contact = new Contact();
    contact.label = req.body.contact.label;
    contact.phoneNumber = req.body.contact.phoneNumber;
    restaurant.contacts = [contact];
    restaurantRepository
      .save(restaurant)
      .then((savedRestaurant) => {
        return restaurantRepository
          .addFoodCategories(req.body.foodCategories, savedRestaurant)
          .then((result) => {
            return res.status(200).json({ message: "Success" });
          })
          .catch((error) => {
            res.status(500).json({ message: "Failed" });
          });
      })
      .catch((err) => {
        res.status(500).json({ message: "Failed" });
      });
  });
};

export const signUpCustomer = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "Failed",
      errors: errors.array().map((err) => err),
    });
  }

  const foodCategoryRepository = getRepository(FoodCategory);
  const customerRepository = getRepository(Customer);
  bcrypt.hash(req.body.password, 12, async (err, hasedPassword) => {
    const customer = new Customer();
    customer.firstName = req.body.firstName;
    customer.lastName = req.body.lastName;
    customer.phoneNumber = req.body.phoneNumber;
    customer.username = req.body.username;
    customer.password = hasedPassword;
    await foodCategoryRepository
      .findByIds(req.body.foodCategories)
      .then((foodCategories) => {
        customer.foodCategories = foodCategories;
      });
    customerRepository
      .save(customer)
      .then((savedCustomer) => {
        if (savedCustomer)
          res.status(200).json({
            message: "Success",
          });
      })
      .catch((err) => {
        res.status(500).json({ message: "Failed" });
      });
  });
};
