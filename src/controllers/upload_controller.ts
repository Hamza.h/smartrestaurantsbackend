import { Request, Response, NextFunction } from "express";
import { v2 as cloudinary } from "cloudinary";
import streamifier from "streamifier";

const destinationFolder: string = "images";

cloudinary.config({
  cloud_name: "smart-restaurants-storage",
  api_key: "283853167933273",
  api_secret: "NWkZKGxZTcZ9Ga_qBXqiIFYVsfY",
});

export const uploadController = function (
  req: Request,
  res: Response,
  next: NextFunction
) {
  let streamUpload = (req: Request) => {
    return new Promise((resolve, reject) => {
      let stream = cloudinary.uploader.upload_stream(
        {
          folder: destinationFolder,
        },
        (error, result) => {
          if (result) {
            resolve(result);
          } else {
            reject(error);
          }
        }
      );

      streamifier.createReadStream(req.file.buffer).pipe(stream);
    });
  };

  streamUpload(req)
    .then((result: any) => {
      console.log(result);
      if (result)
        res.status(200).json({
          imageUrl: result["secure_url"],
        });
    })
    .catch((err) => {
      res.status(500).json({ message: "Failed", error: err.message });
    });
};

export const deleteImage = async (imageUrl: string): Promise<any> => {
  const publicId: string = `${destinationFolder}/${
    imageUrl.split("${destinationFolder}/")[1]
  }`;
  return cloudinary.uploader
    .destroy(publicId, function (result) {
      console.log(result);
    })
    .then((result) => {
      if (result) return true;
    })
    .catch((err) => {
      return false;
    });
};
