import { Request, Response, NextFunction } from "express";
import { getRepository, Not, IsNull, In } from "typeorm";
import CustomerMeal from "../models/customer_meal";
import CustomerRestaurant from "../models/customer_restaurant";
import Customer from "../models/customer";
import bcrypt from "bcrypt";
import Restaurant from "../models/restaurant";
import Contact from "../models/contact";
import RestaurantFoodCategory from "../models/restaurant_food_category";

export const getFavouiteMeals = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(CustomerMeal)
      .find({
        relations: ["meal"],
        where: {
          customer: { id: req.body.customer.id },
          isFavourite: true,
        },
      })
      .then((meals) => {
        return res.status(200).json(meals);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getFavouriteRestaurants = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(CustomerRestaurant)
      .find({
        relations: ["restaurant"],
        where: {
          customer: { id: req.body.customer.id },
          isFavourite: true,
        },
      })
      .then((restaurants) => {
        return res.status(200).json(restaurants);
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

export const editCustomerFoodCategories = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const foodCategories: Array<any> = req.body.foodCategories;
    if (!foodCategories)
      return res.status(400).json({ message: "Missing foodCategories" });
    const finalCustomer = req.body.customer;
    finalCustomer.foodCategories = foodCategories;
    getRepository(Customer)
      .save(finalCustomer)
      .then((result) => {
        return res.status(200).json({ message: "Success" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

export const changePassword = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    if (req.body.customer) {
      bcrypt
        .compare(oldPassword, req.body.customer.password)
        .then((isEqual) => {
          if (!isEqual) {
            return res.status(422).json({ message: "Failed" });
          }
          bcrypt.hash(newPassword, 12, (err, hasedPassword) => {
            req.body.customer.password = hasedPassword;
            getRepository(Customer)
              .save(req.body.customer)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((err) => {
                return res.status(500).json({ message: err.message });
              });
          });
        })
        .catch((err) =>
          res.status(500).json({ message: "Failed", error: err.message })
        );
    } else {
      bcrypt
        .compare(oldPassword, req.body.restaurant.password)
        .then((isEqual) => {
          if (!isEqual) {
            return res.status(422).json({ message: "Failed" });
          }
          bcrypt.hash(newPassword, 12, (err, hasedPassword) => {
            req.body.restaurant.password = hasedPassword;
            getRepository(Restaurant)
              .save(req.body.restaurant)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((err) => {
                return res.status(500).json({ message: err.message });
              });
          });
        })
        .catch((err) =>
          res.status(500).json({ message: "Failed", error: err.message })
        );
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const editCustomerProfile = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const username = req.body.username;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const phoneNumber = req.body.phoneNumber;
    if (!username) return res.status(400).json({ message: "Missing username" });
    if (!firstName)
      return res.status(400).json({ message: "Missing firstName" });
    if (!lastName) return res.status(400).json({ message: "Missing lastName" });
    if (!phoneNumber)
      return res.status(400).json({ message: "Missing phoneNumber" });
    const customerRepository = getRepository(Customer);
    customerRepository
      .findOne({
        where: {
          username: username,
          id: Not(req.body.customer.id),
        },
      })
      .then((result) => {
        if (result) {
          return res
            .status(400)
            .json({ message: "This username is already used" });
        } else {
          getRepository(Restaurant)
            .findOne({
              where: {
                username: username,
              },
            })
            .then((result) => {
              if (result) {
                return res
                  .status(400)
                  .json({ message: "This username is already used" });
              } else {
                req.body.customer.username = username;
                req.body.customer.firstName = firstName;
                req.body.customer.lastName = lastName;
                req.body.customer.phoneNumber = phoneNumber;
                customerRepository
                  .save(req.body.customer)
                  .then((result) => {
                    return res.status(200).json({ message: "Success" });
                  })
                  .catch((error) => {
                    return res.status(500).json({ message: error.message });
                  });
              }
            });
        }
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

export const editRestaurantContacts = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const contacts = req.body.contacts;
    if (!contacts) return res.status(400).json({ message: "Missing contacts" });
    req.body.restaurant.contacts = contacts;
    getRepository(Restaurant)
      .save(req.body.restaurant)
      .then((result) => {
        getRepository(Contact).delete({
          restaurant: { id: IsNull() },
        });
        return res.status(200).json({ message: "Success" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const editRestaurantProfile = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const username = req.body.username;
    const restaurantName = req.body.restaurantName;
    const address = req.body.address;
    const latitude = req.body.latitude;
    const longitude = req.body.longitude;
    const hasTables = req.body.hasTables;
    const hasDelivery = req.body.hasDelivery;
    if (!username) return res.status(400).json({ message: "Missing username" });
    if (!restaurantName)
      return res.status(400).json({ message: "Missing restaurantName" });
    if (!address) return res.status(400).json({ message: "Missing address" });
    if (!latitude) return res.status(400).json({ message: "Missing latitude" });
    if (!longitude)
      return res.status(400).json({ message: "Missing longitude" });
    if (hasTables == null)
      return res.status(400).json({ message: "Missing hasTables" });
    if (hasDelivery == null)
      return res.status(400).json({ message: "Missing hasDelivery" });
    const restaurantRepository = getRepository(Restaurant);
    restaurantRepository
      .findOne({
        where: {
          username: username,
          id: Not(req.body.restaurant.id),
        },
      })
      .then((result) => {
        if (result) {
          return res
            .status(400)
            .json({ message: "This username is already used" });
        } else {
          getRepository(Customer)
            .findOne({
              where: {
                username: username,
              },
            })
            .then((result) => {
              if (result) {
                return res
                  .status(400)
                  .json({ message: "This username is already used" });
              } else {
                req.body.restaurant.username = username;
                req.body.restaurant.restaurantName = restaurantName;
                req.body.restaurant.address = address;
                req.body.restaurant.latitude = latitude;
                req.body.restaurant.longitude = longitude;
                req.body.restaurant.hasTables = hasTables;
                req.body.restaurant.hasDelivery = hasDelivery;
                restaurantRepository
                  .save(req.body.restaurant)
                  .then((result) => {
                    return res.status(200).json({ message: "Success" });
                  })
                  .catch((error) => {
                    return res.status(500).json({ message: error.message });
                  });
              }
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        }
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const editRestaurantFoodCategories = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const addedFoodCategories = req.body.addedFoodCategories;
    const deletedFoodCategories = req.body.deletedFoodCategories;
    if (addedFoodCategories.length == 0 && deletedFoodCategories.length == 0)
      return res.status(400).json({
        message: "Missing addedFoodCategories || deletedFoodCategories",
      });
    const restaurantFoodCategoryRepository = getRepository(
      RestaurantFoodCategory
    );
    const foodCategories = await restaurantFoodCategoryRepository.find({
      relations: ["meals"],
      where: {
        restaurant: { id: req.body.restaurant.id },
        foodCategory: { id: In(deletedFoodCategories.map((e: any) => e.id)) },
      },
    });
    for (let i = 0; i < foodCategories.length; i++) {
      if (foodCategories[i].meals.length > 0)
        return res.status(400).json({
          message: `You Should Delete All Meals Of Food Category ID ${foodCategories[i].id}`,
        });
    }
    if (foodCategories.length > 0)
      await restaurantFoodCategoryRepository.delete(
        foodCategories.map((e) => e.id)
      );
    let wantToAddFoodCategories = [];
    for (let i = 0; i < addedFoodCategories?.length; i++) {
      wantToAddFoodCategories.push({
        restaurant: { id: req.body.restaurant.id },
        foodCategory: addedFoodCategories[i],
      });
    }
    if (wantToAddFoodCategories.length > 0)
      await restaurantFoodCategoryRepository.save(wantToAddFoodCategories);
    return res.status(200).json({ message: "Success" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
