import { Request, Response, NextFunction } from "express";
import { getRepository, Not, In } from "typeorm";
import Order from "../models/order";
import Meal from "../models/meal";
import Offer from "../models/offer";
import {
  PENDING_STATE,
  PROCESSING_STATE,
  DELIVERING_STATE,
  FINISHED_STATE,
  REJECTED_STATE,
} from "../utils/constants";

export const createOrder = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.body.restaurantId;
    const customerLat = req.body.customerLat;
    const customerLong = req.body.customerLong;
    const mealsIDs: Array<any> = req.body.meals;
    const offersIDs: Array<any> = req.body.offers;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    if (!mealsIDs && !offersIDs)
      return res.status(400).json({ message: "Missing meals || offers" });
    let meals: any = [];
    if (mealsIDs)
      meals =
        (await getRepository(Meal).findByIds(mealsIDs.map((e) => e.id))) ?? [];
    let offers: any = [];
    if (offersIDs)
      offers =
        (await getRepository(Offer).findByIds(offersIDs.map((e) => e.id))) ??
        [];
    let orderItems: Array<any> = [];
    for (let i = 0; i < meals?.length; i++)
      orderItems.push({
        meal: meals[i],
        itemPrice: meals[i].mealPrice,
        itemQuantity: mealsIDs.find((e) => e.id == meals[i].id).quantity,
      });
    for (let i = 0; i < offers?.length; i++)
      orderItems.push({
        offer: offers[i],
        itemPrice: offers[i].price,
        itemQuantity: offersIDs.find((e) => e.id == offers[i].id).quantity,
      });
    getRepository(Order)
      .save({
        customer: { id: req.body.customer.id },
        customerLat: customerLat,
        customerLong: customerLong,
        orderDate: new Date(),
        orderTax: 0,
        restaurant: { id: restaurantId },
        orderMeals: orderItems,
        status: { id: PENDING_STATE },
      })
      .then((result) => {
        return res.status(200).json({ message: "Success" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getUnfinishedOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(Order)
      .find({
        relations: [
          "orderMeals",
          "orderMeals.meal",
          "orderMeals.offer",
          "restaurant",
          "status",
        ],
        where: {
          customer: { id: req.body.customer.id },
          status: {
            id: In([PENDING_STATE, PROCESSING_STATE, DELIVERING_STATE]),
          },
        },
      })
      .then((orders) => {
        return res.status(200).json(orders);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getFinishedOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let whereCond: any = {
      status: { id: FINISHED_STATE },
    };
    if (req.body.customer) whereCond["customer"] = { id: req.body.customer.id };
    else whereCond["restaurant"] = { id: req.body.restaurant.id };
    getRepository(Order)
      .find({
        relations: [
          "customer",
          "restaurant",
          "orderMeals",
          "orderMeals.meal",
          "orderMeals.offer",
        ],
        where: whereCond,
      })
      .then((orders) => {
        return res.status(200).json(orders);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getOrderByID = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const orderId: any = req.query.orderId;
    if (!orderId) return res.status(400).json({ message: "Missing orderId" });
    getRepository(Order)
      .findOne(orderId, {
        relations: [
          "orderMeals",
          "orderMeals.meal",
          "orderMeals.offer",
          "customer",
          "restaurant",
        ],
      })
      .then((order) => {
        if (order) return res.status(200).json(order);
        else return res.status(404).json({ message: "Order Not Found" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getPendingOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(Order)
      .find({
        relations: ["customer"],
        where: {
          restaurant: { id: req.body.restaurant.id },
          status: { id: PENDING_STATE },
        },
      })
      .then((orders) => {
        return res.status(200).json(orders);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getProcessingOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(Order)
      .find({
        relations: ["customer"],
        where: {
          restaurant: { id: req.body.restaurant.id },
          status: { id: PROCESSING_STATE },
        },
      })
      .then((orders) => {
        return res.status(200).json(orders);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getDeliveringOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    getRepository(Order)
      .find({
        relations: ["customer"],
        where: {
          restaurant: { id: req.body.restaurant.id },
          status: { id: DELIVERING_STATE },
        },
      })
      .then((orders) => {
        return res.status(200).json(orders);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const changeOrderState = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const orderId = req.body.orderId;
    const state = req.body.state;
    if (!orderId) return res.status(400).json({ message: "Missing orderId" });
    if (!state) return res.status(400).json({ message: "Missing state" });
    const orderRepository = getRepository(Order);
    orderRepository
      .findOne(orderId, {
        where: {
          restaurant: { id: req.body.restaurant.id },
        },
      })
      .then((order: any) => {
        if (order) {
          order.status = { id: state };
          orderRepository
            .save(order)
            .then((result) => {
              return res.status(200).json({ message: " Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "Order Not Found" });
      });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};
