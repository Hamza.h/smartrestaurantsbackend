import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import FoodCategory from "../models/food_category";

export const getFoodCategories = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  getRepository(FoodCategory)
    .find()
    .then((foodCategories) => {
      res.status(200).json(foodCategories);
    })
    .catch((err) => {
      res.status(500).json({ message: "Failed", error: err.message });
    });
};
