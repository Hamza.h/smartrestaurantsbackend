import { Request, Response, NextFunction } from "express";
import { getRepository, In, Like, MoreThanOrEqual } from "typeorm";
import Restaurant from "../models/restaurant";
import CustomerRestaurant from "../models/customer_restaurant";
import RestaurantFoodCategory from "../models/restaurant_food_category";
import Meal from "../models/meal";
import OrderMeal from "../models/order_meal";
import Offer from "../models/offer";

export const searchRestaurant = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const name = req.query.name;
    if (!name) return res.status(400).json({ message: "Missing name" });
    getRepository(Restaurant)
      .find({
        where: {
          restaurantName: Like(`%${name}%`),
        },
      })
      .then((restaurants) => {
        return res.status(200).json(restaurants);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const favouriteRestaurant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.body.restaurantId;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    const customerRestaurantRepository = getRepository(CustomerRestaurant);
    customerRestaurantRepository
      .findOne({
        where: {
          customer: { id: req.body.customer.id },
          restaurant: { id: restaurantId },
        },
      })
      .then((customerRestaurant) => {
        if (customerRestaurant) {
          customerRestaurant.isFavourite = !customerRestaurant.isFavourite;
          customerRestaurantRepository
            .save(customerRestaurant)
            .then((result) => {
              return res.status(200).json({ message: "Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else {
          customerRestaurantRepository
            .save({
              customer: { id: req.body.customer.id },
              restaurant: { id: restaurantId },
              isFavourite: true,
            })
            .then((result) => {
              return res.status(200).json({ message: "Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        }
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const reviewRestaurant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.body.restaurantId;
    const restaurantRate = req.body.restaurantRate;
    const comment = req.body.comment;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    if (!restaurantRate)
      return res.status(400).json({ message: "Missing restaurantRate" });
    if (!comment) return res.status(400).json({ message: "Missing comment" });
    const customerRestaurantRepository = getRepository(CustomerRestaurant);
    customerRestaurantRepository
      .findOne({
        where: {
          customer: { id: req.body.customer.id },
          restaurant: { id: restaurantId },
        },
      })
      .then((customerRestaurant) => {
        if (customerRestaurant && customerRestaurant.restaurantRate) {
          return res
            .status(400)
            .json({ message: "You have been already review this restaurant" });
        } else {
          if (!customerRestaurant) {
            customerRestaurantRepository
              .save({
                customer: { id: req.body.customer.id },
                restaurant: { id: restaurantId },
                restaurantRate: restaurantRate,
                comment: comment,
              })
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                return res.status(500).json({ message: error.message });
              });
          } else {
            customerRestaurant.restaurantRate = restaurantRate;
            customerRestaurant.comment = comment;
            customerRestaurantRepository
              .save(customerRestaurant)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                return res.status(500).json({ message: error.message });
              });
          }
        }
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getAllRestaurants = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const page: any = req.query.page ?? 1;
    const pageCount = 10;
    let restaurantsIDs;
    const customerFoodCategories = req.body.customer.foodCategories.map(
      (e: any) => e.id
    );
    restaurantsIDs = (
      await getRepository(RestaurantFoodCategory)
        .createQueryBuilder()
        .select("restaurant_id as id")
        .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
        .getRawMany()
    ).map((e) => e.id);
    getRepository(Restaurant)
      .createQueryBuilder()
      .select()
      .where("id IN (:ids)", {
        ids: restaurantsIDs,
      })
      .skip((page - 1) * pageCount)
      .take(pageCount)
      .getMany()
      .then((restaurants) => {
        return res.status(200).json(restaurants);
      })
      .catch((err) => res.status(500).json({ message: err.message }));
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getAllReviews = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.query.restaurantId;
    const page: any = req.query.page ?? 1;
    const pageCount = 15;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    getRepository(CustomerRestaurant)
      .find({
        relations: ["customer"],
        where: {
          restaurant: { id: restaurantId },
        },
        skip: (page - 1) * pageCount,
        take: pageCount,
      })
      .then((reviews) => {
        return res.status(200).json(reviews);
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getRestaurantByID = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId: any = req.query.restaurantId;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    const mealRepository = getRepository(Restaurant);
    mealRepository
      .findOne(restaurantId, {
        relations: ["contacts", "reviews", "reviews.customer"],
      })
      .then((restaurant) => {
        if (restaurant) {
          restaurant.reviews = restaurant.reviews.slice(0, 2);
          let deleteReviews: any = [];
          restaurant.reviews.forEach((customerRestaurant: any) => {
            if (req.body.customer.id == customerRestaurant.customer.id)
              Object.assign(restaurant, {
                isFavourite: customerRestaurant.isFavourite,
              });
            if (
              customerRestaurant.restaurantRate != null &&
              customerRestaurant.comment != null
            ) {
              delete customerRestaurant.customer.password;
              delete customerRestaurant.customer.phoneNumber;
              delete customerRestaurant.customer.username;
            } else deleteReviews.push(customerRestaurant);
          });
          restaurant.reviews = restaurant.reviews.filter((e) => {
            if (!deleteReviews.find((element: any) => e.id == element.id))
              return e;
          });
          return res.status(200).json(restaurant);
        } else return res.status(404).json({ message: "Restaurant Not Found" });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getMostPopularRestaurants = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const customerFoodCategories = req.body.customer.foodCategories.map(
      (e: any) => e.id
    );
    const restaurantsIDs = (
      await getRepository(RestaurantFoodCategory)
        .createQueryBuilder()
        .select("restaurant_id")
        .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
        .getRawMany()
    ).map((e) => e.restaurant_id);
    getRepository(Restaurant)
      .find({
        where: {
          id: In(restaurantsIDs),
          rating: MoreThanOrEqual(4),
        },
      })
      .then((restaurants) => {
        return res.status(200).json(restaurants);
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getRestaurantMostOrderedMeals = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.query.restaurantId;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });
    const restaurantFoodCategoryRepository = getRepository(
      RestaurantFoodCategory
    );
    const mealRepository = getRepository(Meal);
    const orderMealRepository = getRepository(OrderMeal);
    let whereCond;
    let whereValue;
    whereCond = "restaurant_id = :id";
    whereValue = {
      id: restaurantId,
    };
    restaurantFoodCategoryRepository
      .createQueryBuilder()
      .where(whereCond, whereValue)
      .distinct(true)
      .getMany()
      .then((restaurantFoodCategories) => {
        if (restaurantFoodCategories.length > 0) {
          mealRepository
            .createQueryBuilder()
            .where("restaurant_food_category_id IN (:selectedIDs)", {
              selectedIDs: restaurantFoodCategories.map((e) => e.id),
            })
            .getMany()
            .then((allMeals) => {
              if (allMeals.length > 0) {
                orderMealRepository
                  .createQueryBuilder()
                  .select("meal_id as mealId")
                  .addSelect("SUM(item_quantity) AS itemQuantity")
                  .where("meal_id IN (:meals)", {
                    meals: allMeals.map((e) => e.id),
                  })
                  .orderBy("itemQuantity", "DESC")
                  .groupBy("meal_id")
                  .getRawMany()
                  .then((items) => {
                    mealRepository
                      .findByIds(
                        items.map((e) => e.mealId),
                        {
                          take: 3,
                        }
                      )
                      .then((meals) => {
                        return res.status(200).json(meals);
                      })
                      .catch((err) => {
                        return res.status(500).json({ message: err.message });
                      });
                  })
                  .catch((error) => {
                    return res.status(500).json({ message: error.message });
                  });
              } else return res.status(404).json({ message: "No Meals Found" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "No Meals Found" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
    mealRepository.find;
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getRestaurantMostOrderedOffers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const restaurantId = req.query.restaurantId;
    if (!restaurantId)
      return res.status(400).json({ message: "Missing restaurantId" });

    const offerRepository = getRepository(Offer);
    const orderMealRepository = getRepository(OrderMeal);
    const whereCond = "restaurant_id = :id";
    let whereValue = { id: restaurantId };
    offerRepository
      .createQueryBuilder()
      .where(whereCond, whereValue)
      .getMany()
      .then((allOffers) => {
        if (allOffers.length > 0) {
          orderMealRepository
            .createQueryBuilder()
            .select("offer_id as offerId")
            .addSelect("SUM(item_quantity) AS itemQuantity")
            .where("offer_id IN (:offers)", {
              offers: allOffers.map((e) => e.id),
            })
            .orderBy("itemQuantity", "DESC")
            .groupBy("offer_id")
            .getRawMany()
            .then((items) => {
              offerRepository
                .findByIds(
                  items.map((e) => e.offerId),
                  {
                    take: 3,
                  }
                )
                .then((offers) => {
                  return res.status(200).json(offers);
                })
                .catch((err) => {
                  return res.status(500).json({ message: err.message });
                });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "No Offers Found" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
