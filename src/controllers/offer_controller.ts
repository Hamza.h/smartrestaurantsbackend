import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import Offer from "../models/offer";
import Meal from "../models/meal";
import RestaurantFoodCategory from "../models/restaurant_food_category";
import OrderMeal from "../models/order_meal";
import { defaultMealOfferImageUrl } from "../utils/constants";

export const createOffer = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const offerName = req.body.offerName;
    const price = req.body.price;
    let imageUrl = req.body.imageUrl;
    const startDate = req.body.startDate;
    const endDate = req.body.endDate;
    const description = req.body.description;
    const mealsIDs: Array<number> = req.body.meals;
    if (!offerName)
      return res.status(400).json({ message: "Missing offerName" });
    if (!price) return res.status(400).json({ message: "Missing price" });
    if (!startDate)
      return res.status(400).json({ message: "Missing startDate" });
    if (!endDate) return res.status(400).json({ message: "Missing endDate" });
    if (!description)
      return res.status(400).json({ message: "Missing description" });
    if (!mealsIDs) return res.status(400).json({ message: "Missing meals" });
    const meals: Array<any> = mealsIDs.map((id) => {
      return { id: id };
    });
    if (!imageUrl) imageUrl = defaultMealOfferImageUrl;
    getRepository(Offer)
      .save({
        offerName: offerName,
        price: price,
        imageUrl: imageUrl,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        description: description,
        restaurant: { id: req.body.restaurant.id },
        meals: meals,
      })
      .then((result) => {
        return res.status(200).json({ message: "Success" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const editOffer = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const offerId = req.body.offerId;
    const offerName = req.body.offerName;
    const price = req.body.price;
    const imageUrl = req.body.imageUrl;
    const startDate = req.body.startDate;
    const endDate = req.body.endDate;
    const description = req.body.description;
    const mealsIDs: Array<number> = req.body.meals;
    if (!offerId) return res.status(400).json({ message: "Missing offerId" });
    if (!offerName)
      return res.status(400).json({ message: "Missing offerName" });
    if (!price) return res.status(400).json({ message: "Missing price" });
    if (!startDate)
      return res.status(400).json({ message: "Missing startDate" });
    if (!endDate) return res.status(400).json({ message: "Missing endDate" });
    if (!description)
      return res.status(400).json({ message: "Missing description" });
    if (!mealsIDs) return res.status(400).json({ message: "Missing meals" });
    const offerRepository = getRepository(Offer);
    offerRepository
      .findOne(offerId, {
        relations: ["meals"],
      })
      .then(async (offer) => {
        if (offer) {
          offer.meals = await getRepository(Meal).findByIds(mealsIDs);
          offer.offerName = offerName;
          offer.price = price;
          offer.description = description;
          offer.startDate = new Date(startDate);
          offer.endDate = new Date(endDate);
          if (imageUrl) offer.imageUrl = imageUrl;
          offerRepository
            .save(offer)
            .then((result) => {
              return res.status(200).json({ message: "Success" });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "Offer Not Found" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const deleteOffer = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const offerId: any = req.query.offerId;
    if (!offerId) return res.status(400).json({ message: "Missing offerId" });
    const offerRepository = getRepository(Offer);
    offerRepository
      .findOne(offerId, {
        relations: ["restaurant"],
      })
      .then((offer) => {
        if (offer) {
          if (offer.restaurant.id == req.body.restaurant.id)
            offerRepository
              .softDelete(offer)
              .then((result) => {
                return res.status(200).json({ message: "Success" });
              })
              .catch((error) => {
                return res.status(500).json({ message: error.message });
              });
        } else return res.status(404).json({ message: "Offer Not Found" });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getOfferByID = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const offerId: any = req.query.offerId;
    if (!offerId) return res.status(400).json({ message: "Missing offerId" });
    const offerRepository = getRepository(Offer);
    offerRepository
      .findOne(offerId, {
        relations: ["meals", "restaurant"],
      })
      .then((offer) => {
        if (offer) return res.status(200).json(offer);
        else return res.status(404).json({ message: "Offer Not Found" });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const searchOffer = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const offerName = req.query.offerName;
    if (!offerName)
      return res.status(400).json({ message: "Missing offerName" });
    getRepository(Offer)
      .createQueryBuilder()
      .select()
      .where("offer_name like :label", { label: `%${offerName}%` })
      .getMany()
      .then((offers) => {
        return res.status(200).json(offers);
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getAllOffers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const page: any = req.query.page ?? 1;
    const pageCount = 10;
    let restaurantsIDs = [];
    if (req.body.restaurant) {
      restaurantsIDs.push(req.body.restaurant.id);
    } else {
      const customerFoodCategories = req.body.customer.foodCategories.map(
        (e: any) => e.id
      );
      restaurantsIDs = (
        await getRepository(RestaurantFoodCategory)
          .createQueryBuilder()
          .select("restaurant_id")
          .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
          .getRawMany()
      ).map((e) => e.restaurant_id);
    }
    getRepository(Offer)
      .createQueryBuilder("offer")
      .leftJoinAndSelect("offer.restaurant", "restaurant")
      .select()
      .where("restaurant_id IN (:ids)", {
        ids: restaurantsIDs,
      })
      .skip((page - 1) * pageCount)
      .take(pageCount)
      .getMany()
      .then((meals) => {
        return res.status(200).json(meals);
      })
      .catch((err) => res.status(500).json({ message: err.message }));
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getMostOrderedOffers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let restaurantsIDs = [];
    if (req.body.restaurant) {
      restaurantsIDs.push(req.body.restaurant.id);
    } else {
      const customerFoodCategories = req.body.customer.foodCategories.map(
        (e: any) => e.id
      );
      restaurantsIDs = (
        await getRepository(RestaurantFoodCategory)
          .createQueryBuilder()
          .select("restaurant_id")
          .where("food_category_id IN (:ids)", { ids: customerFoodCategories })
          .getRawMany()
      ).map((e) => e.restaurant_id);
    }
    const offerRepository = getRepository(Offer);
    const orderMealRepository = getRepository(OrderMeal);
    const whereCond = "restaurant_id IN (:ids)";
    let whereValue = { ids: restaurantsIDs };
    offerRepository
      .createQueryBuilder()
      .where(whereCond, whereValue)
      .getMany()
      .then((allOffers) => {
        if (allOffers.length > 0) {
          orderMealRepository
            .createQueryBuilder()
            .select("offer_id as offerId")
            .addSelect("SUM(item_quantity) AS itemQuantity")
            .where("offer_id IN (:offers)", {
              offers: allOffers.map((e) => e.id),
            })
            .orderBy("itemQuantity", "DESC")
            .groupBy("offer_id")
            .getRawMany()
            .then((items) => {
              offerRepository
                .findByIds(
                  items.map((e) => e.offerId),
                  {
                    take: 3,
                  }
                )
                .then((offers) => {
                  return res.status(200).json(offers);
                })
                .catch((err) => {
                  return res.status(500).json({ message: err.message });
                });
            })
            .catch((error) => {
              return res.status(500).json({ message: error.message });
            });
        } else return res.status(404).json({ message: "No Offers Found" });
      })
      .catch((error) => {
        return res.status(500).json({ message: error.message });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
