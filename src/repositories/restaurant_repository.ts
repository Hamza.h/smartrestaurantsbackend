import { EntityRepository, Repository, getRepository } from "typeorm";
import Restaurant from "../models/restaurant";
import FoodCategory from "../models/food_category";
import RestaurantFoodCategory from "../models/restaurant_food_category";

@EntityRepository(Restaurant)
export class RestaurantRepository extends Repository<Restaurant> {
  addFoodCategories(ids: number[], restaurant: Restaurant) {
    const foodCategoryRepository = getRepository(FoodCategory);
    const restaurantFoodCategoryRepository = getRepository(
      RestaurantFoodCategory
    );
    return foodCategoryRepository.findByIds(ids).then((foodCategories) => {
      foodCategories.forEach(async (foodCategory) => {
        const restaurantFoodCategory = new RestaurantFoodCategory();
        restaurantFoodCategory.foodCategory = foodCategory;
        restaurantFoodCategory.restaurant = restaurant;
        await restaurantFoodCategoryRepository.save(restaurantFoodCategory);
      });
    });
  }

  async getAllFoodCategories(restaurant: Restaurant) {
    const foodCategoryRepository = getRepository(FoodCategory);
    const restaurantFoodCategoryRepository = getRepository(
      RestaurantFoodCategory
    );
    return await restaurantFoodCategoryRepository
      .find({
        where: {
          restaurant: restaurant,
        },
        relations: ["foodCategory"],
      })
      .then(async (restaurantFoodCategories) => {
        return await foodCategoryRepository.findByIds(
          restaurantFoodCategories.map(
            (restaurantFoodCategory) => restaurantFoodCategory.foodCategory
          )
        );
      });
  }
}
