import { Request, Response, NextFunction, json } from "express";
import multer from "multer";
const uploader = multer();

const middlewares = [json(), uploader.none()];

export const CROS_MIDDLEWARE = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  console.log("Allow CROS");
  next();
};

export default middlewares;
