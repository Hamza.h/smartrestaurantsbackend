import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import Customer from "../models/customer";
import Restaurant from "../models/restaurant";
import { encryptSecretKey } from "../utils/constants";

export const isAuth = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token: string = req.get("Authorization")?.split(" ")[1] ?? "";
  if (token === "")
    return res
      .status(400)
      .json({ message: "Missing bearer token in request header" });
  let decodedToken: any;
  try {
    decodedToken = jwt.verify(token, encryptSecretKey);
  } catch (err) {
    return res.status(401).json({ message: "Unauthorized" });
  }
  if (!decodedToken) return res.status(401).json({ message: "Unauthorized" });
  const isCustomer = decodedToken.isCustomer;
  if (isCustomer) {
    const customer = await getRepository(Customer).findOne({
      relations: ["foodCategories"],
      where: { username: decodedToken.username },
    });

    if (!customer) return res.status(404).json({ message: "Not Found" });

    Object.assign(req.body, { customer: customer });
  } else {
    const restaurant = await getRepository(Restaurant).findOne({
      where: { username: decodedToken.username },
    });

    if (!restaurant) return res.status(404).json({ message: "Not Found" });

    Object.assign(req.body, { restaurant: restaurant });
  }

  next();
};
