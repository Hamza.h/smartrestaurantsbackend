import express from "express";
import appRoutes from "./routes/app_router";
import databaseConnection from "./utils/database";
import { CROS_MIDDLEWARE } from "./middlewares/basics";

const app = express();

app.use(CROS_MIDDLEWARE);

app.get("/favicon.ico", (req, res) => res.status(204));

app.use(appRoutes);

app.use("/", (req, res, next) => {
  res.status(404).json({ message: "no such route" });
});

databaseConnection.then((res) => app.listen(process.env.PORT || 3000));
